/**
 * @file    StereoCalibration.cpp
 * @brief   Calibrate stereo cameras
 * @author  Yao Chen
 * @date    July 18, 2016
 */

#include "StereoCalibration.h"
#include "MonoCalibrationFactor.h"
#include "MonoCalibrationFactor2.h"

#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/Marginals.h>

#include <boost/make_shared.hpp>

#include <iostream>

using namespace std;
using namespace gtsam;
using namespace gtsam::noiseModel;

using symbol_shorthand::B;
using symbol_shorthand::K;
using symbol_shorthand::X;

/* Initialize the parameters from monocular calibration */
boost::tuple<Cal3DS2, Cal3DS2, map<size_t, Pose3>, Pose3>
StereoCalibration::init(const Values& leftResult, const Values& rightResult) {

    // insert camera calibrations
    if (!leftResult.exists(K(0)) || !rightResult.exists(K(0))) {
        throw runtime_error("Invalid mono calibration results!");
    }
    const Cal3DS2 K0 = leftResult.at<Cal3DS2>(K(0));
    const Cal3DS2 K1 = rightResult.at<Cal3DS2>(K(0));

    // add between poses and left camera poses if both left and right camera poses exist
    size_t nrBetweenPoses = 0;
    Vector6 left_T_right_vector = gtsam::Z_6x1;
    map<size_t, Pose3> leftCamPoses;
    for (size_t k = 0; k < nrImages_; k++) {
        if (leftResult.exists(X(k)) && rightResult.exists(X(k))) {
            const Pose3 world_T_left = leftResult.at<Pose3>(X(k));
            const Pose3 world_T_right = rightResult.at<Pose3>(X(k));
            leftCamPoses.emplace(k, world_T_left);

            const Pose3 left_T_right = world_T_left.between(world_T_right);
            left_T_right_vector = left_T_right_vector + Pose3::Logmap(left_T_right);
            nrBetweenPoses++;
        }
    }

    left_T_right_vector = left_T_right_vector / static_cast<double>(nrBetweenPoses);
    const Pose3 estimated_left_T_right  = Pose3::Expmap(left_T_right_vector);

    return boost::make_tuple(K0, K1, leftCamPoses, estimated_left_T_right);
}

/* Calibrate stereo cameras */
boost::tuple<Values, NonlinearFactorGraph>
StereoCalibration::calibrateCameras(const gtsam::Cal3DS2& K0, const gtsam::Cal3DS2& K1,
                                    const std::map<std::size_t, gtsam::Pose3>& leftCamPoses,
                                    const gtsam::Pose3& betweenPose,
                                    const std::vector<gtsam::Point2>& corners3D,
                                    const std::vector<std::vector<gtsam::Point2> >& leftCorners2D,
                                    const std::vector<std::vector<gtsam::Point2> >& rightCorners2D) {

    // create a nonlinear factor graph
    NonlinearFactorGraph graph;

    // add measurement noise
    SharedDiagonal measurementNoise = Diagonal::Sigmas(Vector2(1, 1));

    // add factors to the graph
    // MonoCalibrationFactor measures the reprojection error of the left camera
    // MonoCalibrationFactor2 measures the reprojection error of the right camera. However, the pose of the right camera
    // is calculated by left camera pose's composing the between pose.
    for (size_t i = 0; i < nrImages_; i++) {
        if (leftCamPoses.find(i) != leftCamPoses.end()) {
            assert(leftCorners2D[i].size() == rightCorners2D[i].size());
            for (size_t j = 0; j < leftCorners2D[i].size(); j++) {
                const Point3 point3D(corners3D[j].x(), corners3D[j].y(), 0.0);
                graph.push_back(boost::make_shared<MonoCalibrationFactor>(measurementNoise, X(i), K(0), point3D, leftCorners2D[i][j]));
                graph.push_back(boost::make_shared<MonoCalibrationFactor2>(measurementNoise, X(i), B(0), K(1), point3D, rightCorners2D[i][j]));
            }
        }
    }

    // add initial estimates
    Values initial;
    initial.insert(K(0), K0);   // left camera calibration
    initial.insert(K(1), K1);   // right camera calibration
    initial.insert(B(0), betweenPose);  // initial between pose
    for (const auto& entry: leftCamPoses) {
        const size_t i = entry.first;
        const Pose3 pose = entry.second;
        initial.insert(X(i), pose); // initial poses of the left camera
    }

    // optimize the graph using Levenberg-Marquardt
    LevenbergMarquardtParams LMParams;
    LMParams.setVerbosityLM("TRYLAMBDA");
    Values result = LevenbergMarquardtOptimizer(graph, initial, LMParams).optimize();

    // return result and factor graph
    return boost::make_tuple(result, graph);
}
