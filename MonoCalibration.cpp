/**
 * @file    MonoCalibration.cpp
 * @brief   Calibrate a monocular camera.
 * @cite    Zhengyou Zhang: A Flexible New Technique for Camera Calibration.
 *          IEEE Trans. Pattern Anal. Mach. Intell. (PAMI), 22(11): 1330-1334, 2000.
 * @author  Yao Chen
 * @date    July 18, 2016
 */

#include "MonoCalibration.h"
#include "MonoCalibrationFactor.h"

#include <gtsam/inference/Symbol.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>

#include <boost/make_shared.hpp>

using namespace std;
using namespace gtsam;
using namespace gtsam::noiseModel;

using symbol_shorthand::K;
using symbol_shorthand::X;
using symbol_shorthand::Z;

/* Normalize the data using a linear transformation T with P3' = T * P3 */
boost::tuple<vector<Point2>, Matrix3> MonoCalibration::normalizePoints(const vector<Point2>& points) {
    // 1. find the centroid of all 3D points
    Point2 centroid;
    for(const Point2& point: points) {
        centroid += point;
    }
    centroid = centroid / points.size();

    // 2. center the points and find the average distance
    vector<Point2> normalizedPoints;
    double dist = 0.0;
    for(const Point2& point: points) {
        Point2 centeredPoint = point - centroid;
        normalizedPoints.push_back(centeredPoint);
        dist += centeredPoint.norm();
    }
    dist = dist / points.size();

    // 3. calculate the scale factor
    double scale = sqrt(2.0) / dist;

    // scale the 3D points
    for(Point2& point: normalizedPoints) {
        point *= scale;
    }

    // 4. calculate the transformation matrix T
    Matrix3 T;
    T << scale,   0.0,  -scale * centroid.x(),
           0.0, scale,  -scale * centroid.y(),
           0.0,   0.0,                    1.0;

    // 5. return the normalized points and the transformation matrix
    return boost::make_tuple(normalizedPoints, T);
}

/* Estimate a homography given one set of 3D-2D correspondencies
 *
 * A = [-P1x -P1y -1 0 0 0 p1x*P1x p1x*P1y p1x
 *      0 0 0 -P1x -P1y -1 p1y*P1x p1y*P1y p1y
 *                    ...
 *                    ...
 *      -Pnx -Pny -1 0 0 0 pnx*Pnx pnx*Pny pnx
 *      0 0 0 -Pnx -Pny -1 pny*Pnx pny*Pny pny]
 *
 * where Pi denotes the i-th 3D point (on the rig) and
 * pi denotes the corresponding i-th 2D point in the image.
 * Hence, A is a 2n-by-9 matrix.
 */
Matrix3 MonoCalibration::estimateHomography(const vector<Point2>& points3D, const vector<Point2>& points2D) {
    // 1. check whether the lengths of the two vectors are the same
    assert(points3D.size() == points2D.size());

    // 2. construct the constraint matrix A
    Matrix A = Matrix::Zero(2 * points3D.size(), 9);
    for (size_t k = 0; k < points3D.size(); k++) {
        int i = 2 * k;

        A(i, 0) = -points3D[k].x();
        A(i, 1) = -points3D[k].y();
        A(i, 2) = -1.0;
        A(i, 3) = 0.0;
        A(i, 4) = 0.0;
        A(i, 5) = 0.0;
        A(i, 6) = points2D[k].x() * points3D[k].x();
        A(i, 7) = points2D[k].x() * points3D[k].y();
        A(i, 8) = points2D[k].x();

        A(i + 1, 0) = 0.0;
        A(i + 1, 1) = 0.0;
        A(i + 1, 2) = 0.0;
        A(i + 1, 3) = -points3D[k].x();
        A(i + 1, 4) = -points3D[k].y();
        A(i + 1, 5) = -1.0;
        A(i + 1, 6) = points2D[k].y() * points3D[k].x();
        A(i + 1, 7) = points2D[k].y() * points3D[k].y();
        A(i + 1, 8) = points2D[k].y();
    }

    // 3. apply DLT
    size_t rank;
    double minSingularVal;
    Vector9 h;
    boost::tie(rank, minSingularVal, h) = DLT(A);

    // 4. convert the vector into a matrix
    Matrix3 H;
    H << h(0), h(1), h(2),
         h(3), h(4), h(5),
         h(6), h(7), h(8);

    // 5. return the homography matrix
    return H;
}

/* Optimize the homography by minimizing the sum of reprojection errors
 * Obj = \min\sum ||f(H, P)-pp||_2^2, where P = [Px Py 1]' and pp = [ppx ppy]'.
 * f(P) maps P to a 2D point as f(P) = [px/pz py/pz]', where p = H*P.
 * The error is approximated by e = f(H0, P) - pp = J*dH. Hence, the linear
 * update is dH = inv(J'*J)*J'*e.
 */
Matrix3 MonoCalibration::optimizeHomography(const vector<Point2>& points3D, const vector<Point2>& points2D,
                                           const gtsam::Matrix3& H0) {
    // 1. convert the homography to vector whose last entry is 1
    Vector8 h;
    h << H0(0, 0), H0(0, 1), H0(0, 2), H0(1, 0), H0(1, 1), H0(1, 2), H0(2, 0), H0(2, 1);
    if (H0(2, 2) < 1e-8) {
        return H0;
    }

    // 2. normalize the vector by the last entry (h33)
    h = h / H0(2, 2);
    Matrix3 H;
    H << h(0), h(1), h(2), h(3), h(4), h(5), h(6), h(7), h(8), 1.0;

    // 3. nonlinear optimization
    // This is a simple implementation of nonlinear gradient descent.
    // TODO: design a factor and a class to tackle it.
    for (int iter = 0; iter < 10; iter++) {
        // create the Jacobian matrix and the error vector
        Matrix J = Matrix::Zero(2 * points3D.size(), 8);    // Jacobian matrix (2N*8)
        Vector error = Vector::Zero(2 * points3D.size());   // error vector (2N)

        for (size_t k = 0; k < points3D.size(); k++) {
            double Px = points3D[k].x();
            double Py = points3D[k].y();
            Point3 P(Px, Py, 1.0);

            Point3 p = H * P;
            double px = p.x();
            double py = p.y();
            double pz = p.z();

            if (pz < 1e-8) {
                continue;
            }

            double ppx = points2D[k].x();
            double ppy = points2D[k].y();

            int i = 2 * k;

            J(i, 0) = Px / pz;
            J(i, 1) = Py / pz;
            J(i, 2) = 1.0 / pz;
            J(i, 3) = 0.0;
            J(i, 4) = 0.0;
            J(i, 5) = 0.0;
            J(i, 6) = -Px * px / (pz * pz);
            J(i, 7) = -Py * px / (pz * pz);

            J(i + 1, 0) = 0.0;
            J(i + 1, 1) = 0.0;
            J(i + 1, 2) = 0.0;
            J(i + 1, 3) = Px / pz;
            J(i + 1, 4) = Py / pz;
            J(i + 1, 5) = 1.0 / pz;
            J(i + 1, 6) = -Px * py / (pz * pz);
            J(i + 1, 7) = -Py * py / (pz * pz);

            error(i) = px / pz - ppx;
            error(i + 1) = py / pz - ppy;
        }

        // 4. skip the ill-formed case
        if (J.sum() == 0.0) {
            return H0;
        }

        // 5. linear update
        Vector8 update = J.colPivHouseholderQr().solve(error);
        h = h - update;

        // 6. reconstruct H
        H(0, 0) = h(0);
        H(0, 1) = h(1);
        H(0, 2) = h(2);
        H(1, 0) = h(3);
        H(1, 1) = h(4);
        H(1, 2) = h(5);
        H(2, 0) = h(6);
        H(2, 1) = h(7);
        H(2, 2) = 1.0;
    }

    // 7. return the optimized homography matrix
    return H;
}

/* Estimate *ALL* Homographies */
map<size_t, Matrix3> MonoCalibration::estimateHomographies(const vector<Point2>& corners3D,
                                                          const vector<vector<Point2> >& corners2D) {
    // 1. nomalize 3D points
    vector<Point2> normalized3DPoints;
    Matrix3 T1;
    boost::tie(normalized3DPoints, T1) = normalizePoints(corners3D);

    // 2. create the output
    map<size_t, gtsam::Matrix3> homographies;

    for (size_t i = 0; i < corners2D.size(); i++) {
        // 0) skip the degenerate case
        if (corners2D[i].size() <= 4) {
            continue;
        }

        // 1) normalize 2D meaurements
        vector<Point2> normalized2DPoints;
        Matrix3 T2;
        boost::tie(normalized2DPoints, T2) = normalizePoints(corners2D[i]);

        // 2) estimate homography
        Matrix3 HH = estimateHomography(normalized3DPoints, normalized2DPoints);
        Matrix3 tempH = T2.inverse() * HH * T1;

        // 3) optimize homography by minimizing the sum of reprojection errors
        Matrix3 H = optimizeHomography(corners3D, corners2D[i], tempH);

        // 4) store the homography
        homographies.emplace(i, H);
    }

    // 3. return homographies
    return homographies;
}

/* Estimate intrinsic matrix with skew included (5DOFs)
 * Equations (5) - (9) in [Zhang00tpami]
 */
Matrix3 MonoCalibration::estimateIntrinsics(const map<size_t, Matrix3>& homographies) {
    // 1. construct the constraint matrix A
    Matrix A = Matrix::Zero(2 * homographies.size(), 6);

    int k = 0;
    for (const auto& entry: homographies) {
        Matrix3 H = entry.second.transpose();

        int i = 2 * k;

        A(i, 0) = H(0, 0) * H(1, 0);
        A(i, 1) = H(0, 0) * H(1, 1) + H(0, 1) * H(1, 0);
        A(i, 2) = H(0, 1) * H(1, 1);
        A(i, 3) = H(0, 2) * H(1, 0) + H(0, 0) * H(1, 2);
        A(i, 4) = H(0, 2) * H(1, 1) + H(0, 1) * H(1, 2);
        A(i, 5) = H(0, 2) * H(1, 2);

        A(i + 1, 0) = H(0, 0) * H(0, 0) - H(1, 0) * H(1, 0);
        A(i + 1, 1) = H(0, 0) * H(0, 1) + H(0, 1) * H(0, 0) - H(1, 0) * H(1, 1) - H(1, 1) * H(1, 0);
        A(i + 1, 2) = H(0, 1) * H(0, 1) - H(1, 1) * H(1, 1);
        A(i + 1, 3) = H(0, 2) * H(0, 0) + H(0, 0) * H(0, 2) - H(1, 2) * H(1, 0) - H(1, 0) * H(1, 2);
        A(i + 1, 4) = H(0, 2) * H(0, 1) + H(0, 1) * H(0, 2) - H(1, 2) * H(1, 1) - H(1, 1) * H(1, 2);
        A(i + 1, 5) = H(0, 2) * H(0, 2) - H(1, 2) * H(1, 2);

        k++;
    }

    // 2. apply DLT
    size_t rank;
    double minSingularVal;
    Vector6 b;
    boost::tie(rank, minSingularVal, b) = DLT(A);

    // 3. calculate the parameters of B ~ inv(A') * A
    double B11 = b(0);
    double B12 = b(1);
    double B22 = b(2);
    double B13 = b(3);
    double B23 = b(4);
    double B33 = b(5);

    double v0 = (B12*B13 - B11*B23) / (B11*B22 - B12*B12);
    double lambda = B33 - (B13*B13 + v0*(B12*B13 - B11*B23)) / B11;
    double fx = sqrt(lambda / B11);
    double fy = sqrt(lambda*B11 / (B11*B22 - B12*B12));
    double s = -B12 * fx * fx * fy / lambda;
    double u0 = s * v0 / fy - B13 * fx * fx / lambda;

    // 4. create an intrinsic matrix
    Matrix3 K;
    K <<  fx,   s,  u0,
         0.0,  fy,  v0,
         0.0, 0.0, 1.0;

    // 5. return the intrinsic matrix
    return K;
}

/* Estimate intrinsic matrix of the camera without skew (4DOFs)
 * Equations (5) - (9) in [Zhang00tpami]
 */
Matrix3 MonoCalibration::estimateIntrinsics2(const map<size_t, Matrix3>& homographies) {
    // 1. construct the constraint matrix A
    Matrix A = Matrix::Zero(2 * homographies.size(), 5);

    int k = 0;
    for (const auto& entry: homographies) {
        Matrix H = entry.second.transpose();

        int i = 2 * k;

        A(i, 0) = H(0, 0) * H(1, 0);
        A(i, 1) = H(0, 1) * H(1, 1);
        A(i, 2) = H(0, 2) * H(1, 0) + H(0, 0) * H(1, 2);
        A(i, 3) = H(0, 2) * H(1, 1) + H(0, 1) * H(1, 2);
        A(i, 4) = H(0, 2) * H(1, 2);

        A(i + 1, 0) = H(0, 0) * H(0, 0) - H(1, 0) * H(1, 0);
        A(i + 1, 1) = H(0, 1) * H(0, 1) - H(1, 1) * H(1, 1);
        A(i + 1, 2) = H(0, 2) * H(0, 0) + H(0, 0) * H(0, 2) - H(1, 2) * H(1, 0) - H(1, 0) * H(1, 2);
        A(i + 1, 3) = H(0, 2) * H(0, 1) + H(0, 1) * H(0, 2) - H(1, 2) * H(1, 1) - H(1, 1) * H(1, 2);
        A(i + 1, 4) = H(0, 2) * H(0, 2) - H(1, 2) * H(1, 2);

        k++;
    }

    // 2. apply DLT
    size_t rank;
    double minSingularVal;
    Vector5 b;
    boost::tie(rank, minSingularVal, b) = DLT(A);

    // 3. calculate the parameters of B = lambda * inv(A') * A
    double B11 = b(0);
    double B12 = 0.0;
    double B22 = b(1);
    double B13 = b(2);
    double B23 = b(3);
    double B33 = b(4);

    double v0 = (B12*B13 - B11*B23) / (B11*B22 - B12*B12);
    double lambda = B33 - (B13*B13 + v0*(B12*B13 - B11*B23)) / B11;
    double fx = sqrt(lambda / B11);
    double fy = sqrt(lambda*B11 / (B11*B22 - B12*B12));
    double s = -B12 * fx * fx * fy / lambda;
    double u0 = s * v0 / fy - B13 * fx * fx / lambda;

    // 4. create an intrinsic matrix
    Matrix3 K;
    K <<  fx, 0.0,  u0,
         0.0,  fy,  v0,
         0.0, 0.0, 1.0;

    // 5. return the intrinsic matrix
    return K;
}

/* Estimate intrinsic matrix of the camera without skew (4DOFs) using orthognality of vanishing points */
Matrix3 MonoCalibration::estimateIntrinsics2VanishingPoint(const map<size_t, Matrix3>& homographies) {
    // initial estimates of u0 and v0 come from the image size
    double u0 = (static_cast<double>(1280) - 1.0) / 2.0;
    double v0 = (static_cast<double>(1024) - 1.0) / 2.0;

    // cancel out the offset
    Matrix3 M;
    M << 1.0, 0.0, -u0, 0.0, 1.0, -v0, 0.0, 0.0, 1.0;

    // create the matrices
    Matrix AA = Matrix::Zero(2 * homographies.size(), 2);
    Vector bb = Vector::Zero(2 * homographies.size());
    int k = 0;
    for (const auto& entry: homographies) {
        Matrix3 H = M * entry.second;

        Point3 V1 = H.col(0);
        Point3 V2 = H.col(1);
        Point3 V3 = (V1 + V2) / 2.0;
        Point3 V4 = (V1 - V2) / 2.0;

        V1 = V1.normalize();
        V2 = V2.normalize();
        V3 = V3.normalize();
        V4 = V4.normalize();

        double x1 = V1.x();
        double y1 = V1.y();
        double z1 = V1.z();

        double x2 = V2.x();
        double y2 = V2.y();
        double z2 = V2.z();

        double x3 = V3.x();
        double y3 = V3.y();
        double z3 = V3.z();

        double x4 = V4.x();
        double y4 = V4.y();
        double z4 = V4.z();

        int i = 2 * k;

        AA(i, 0) = x1 * x2;
        AA(i, 1) = y1 * y2;
        AA(i + 1, 0) = x3 * x4;
        AA(i + 1, 1) = y3 * y4;

        bb(i) = -z1 * z2;
        bb(i + 1) = -z3 * z4;

        k++;
    }

    bool twoFocalInit = false;
    if (!twoFocalInit) {
       double val = bb.dot(AA.rowwise().sum());
       if (val < 0.0) {
           twoFocalInit = true;
       }
    }

    double fx = 0.0;
    double fy = 0.0;
    if (twoFocalInit) {
        Vector2 temp = AA.colPivHouseholderQr().solve(bb);
        fx = sqrt(fabs(1.0 / temp(0)));
        fy = sqrt(fabs(1.0 / temp(1)));
    } else {
        double val1 = bb.dot(AA.rowwise().sum());
        double val2 = bb.dot(bb);
        fx = sqrt(val1 / val2);
        fy = fx;
    }

    fx = (fx + fy) / 2.0;
    fy = fx;

    // create an intrinsic matrix
    Matrix3 K;
    K << fx, 0, u0,
         0, fy, v0,
         0,  0,  1;

    return K;
}

/* Estimate camera poses */
map<size_t, Pose3> MonoCalibration::estimateExtrinsics(const Matrix3& initK, const map<size_t, Matrix3>& homographies) {
    // 1. create a container of all poses
    map<size_t, Pose3> initCamPoses;
    // 2. loop to calculate the poses
    for (const auto& entry: homographies) {
        // 1) get the homography matrix H
        Matrix3 H = entry.second;
        size_t index = entry.first;

        // 2) calculate the initial rotation matrix
        Point3 r1 = initK.inverse() * H.col(0);
        double scale1 = r1.norm();
        r1 = r1 / scale1;

        Point3 r2 = initK.inverse() * H.col(1);
        double scale2 = r2.norm();
        r2 = r2 / scale2;

        Point3 r3 = r1.cross(r2);

        // 3) refine the rotation matrix
        Rot3 rot1(r1, r2, r3);
        Matrix3 rotation1 = rot1.matrix();

        Matrix U;
        Matrix V;
        Vector S;
        svd(rotation1, U, S, V);

        Matrix3 rotation = U * V.transpose();
        Rot3 rot(rotation);

        // 4) calculate the translation vector
        Point3 translation = initK.inverse() * H.col(2) / (scale1 + scale2) * 2.0;

        // 5) create the pose
        Pose3 pose(rot, translation);

        // 6) discard the degenerate pose
        if (pose.translation().z() <= 0.0) {
            continue;
        }

        // 7) *camera* pose
        initCamPoses.emplace(index, pose.inverse());
    }

    return initCamPoses;
}

/* Calibrate monocular camera */
boost::tuple<Values, Values, NonlinearFactorGraph> MonoCalibration::calibrateCamera(const map<size_t, Pose3>& initCamPoses,
                                                                                    const Matrix3& initK, const vector<Point2>& corners3D,
                                                                                    const vector<vector<Point2> >& corners2D) {
    // create a nonlinear factor graph
    NonlinearFactorGraph graph;

    // add the measurement noise
    SharedDiagonal measurementNoise = Diagonal::Sigmas(Vector2(1, 1));

    // add factors to the graph
    for (size_t i = 0; i < corners2D.size(); i++) {
        if (initCamPoses.find(i) != initCamPoses.end()) {
            for (size_t j = 0; j < corners2D[i].size(); j++) {
                Point3 point3D(corners3D[j].x(), corners3D[j].y(), 0.0);
                graph.push_back(boost::make_shared<MonoCalibrationFactor>(measurementNoise, X(i), K(0), point3D, corners2D[i][j]));
            }
        }
    }
    Cal3DS2 calib(initK(0,0), initK(1, 1), initK(0, 1), initK(0, 2), initK(1, 2), 0.0, 0.0);
//    Cal3DS2 calib1(initK(0,0), 0, initK(0, 1), 640, 512, 0.0, 0.0);
//    Diagonal::shared_ptr calNoise = Diagonal::Sigmas((Vector(9) << 50, 50, 1, 50, 50, 1, 1, 1e-5, 1e-5).finished());
//    graph.push_back(PriorFactor<Cal3DS2>(K(0), calib, calNoise));

    // add initial estimates
    Values initial;
    initial.insert(K(0), calib);
    for (const auto& entry: initCamPoses) {
        size_t i = entry.first;
        Pose3 pose = entry.second;
        initial.insert(X(i), pose);
    }

    // optimize the graph using Levenberg-Marquardt
    LevenbergMarquardtParams lmParams;
    lmParams.lambdaInitial = 1;
    lmParams.lambdaFactor = 10;
    lmParams.maxIterations = 100;
    lmParams.relativeErrorTol = 1e-5;
    lmParams.absoluteErrorTol = 1.0;
    lmParams.verbosityLM = LevenbergMarquardtParams::TRYLAMBDA;
    lmParams.verbosity = NonlinearOptimizerParams::ERROR;
    lmParams.linearSolverType = SuccessiveLinearizationParams::MULTIFRONTAL_CHOLESKY;
    Values result = LevenbergMarquardtOptimizer(graph, initial, lmParams).optimize();

    return boost::make_tuple(initial, result, graph);
}

/* Calibrate monocular camera */
boost::tuple<Values, Values, NonlinearFactorGraph> MonoCalibration::calibrateCamera2(const map<size_t, Pose3>& initCamPoses,
                                                                                     const Matrix3& initK, const vector<Point2>& corners3D,
                                                                                     const vector<vector<Point2> >& corners2D) {
    // create a nonlinear factor graph
    NonlinearFactorGraph graph;

    // add the measurement noisen and prior noise
    SharedDiagonal measurementNoise = Diagonal::Sigmas(Vector2(0.2, 0.2));
    SharedDiagonal priorNoise = Diagonal::Sigmas(Vector2(1, 1));

    // add factors to the graph
    for (size_t i = 0; i < corners2D.size(); i++) {
        if (initCamPoses.find(i) != initCamPoses.end()) {
            for (size_t j = 0; j < corners2D[i].size(); j++) {
                Point3 point3D(corners3D[j].x(), corners3D[j].y(), 0.0);
                graph.push_back(boost::make_shared<MonoCalibrationFactor3>(measurementNoise, X(i), K(0), Z(i*40+j), point3D));
                graph.push_back(boost::make_shared<PriorFactor<Point2> >(Z(i*40+j), corners2D[i][j], priorNoise));
            }
        }
    }

    // add initial estimates
    Values initial;

    Cal3DS2 calib(initK(0,0), initK(1, 1), initK(0, 1), initK(0, 2), initK(1, 2), 0.0, 0.0);
    initial.insert(K(0), calib);

    for (const auto& entry: initCamPoses) {
        size_t i = entry.first;
        Pose3 pose = entry.second;
        initial.insert(X(i), pose);
    }

    for (size_t i = 0; i < corners2D.size(); i++) {
        if (initCamPoses.find(i) != initCamPoses.end()) {
            for (size_t j = 0; j < corners2D[i].size(); j++) {
                initial.insert(Z(i*40+j), corners2D[i][j]);
            }
        }
    }

    // optimize the graph using Levenberg-Marquardt
    LevenbergMarquardtParams lmParams;
    lmParams.lambdaInitial = 1;
    lmParams.lambdaFactor = 10;
    lmParams.maxIterations = 100;
    lmParams.relativeErrorTol = 1e-5;
    lmParams.absoluteErrorTol = 1.0;
    lmParams.verbosityLM = LevenbergMarquardtParams::TRYLAMBDA;
    lmParams.verbosity = NonlinearOptimizerParams::ERROR;
    lmParams.linearSolverType = SuccessiveLinearizationParams::MULTIFRONTAL_CHOLESKY;
    Values result = LevenbergMarquardtOptimizer(graph, initial, lmParams).optimize();

    return boost::make_tuple(initial, result, graph);
}

