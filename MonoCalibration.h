/**
 * @file    MonoCalibration.h
 * @brief   Calibrate a monocular camera based on [Zhang00tpami]
 * @cite    Zhengyou Zhang: A Flexible New Technique for Camera Calibration.
 *          IEEE Trans. Pattern Anal. Mach. Intell. (PAMI), 22(11): 1330-1334, 2000.
 * @author  Yao Chen
 * @date    July 18, 2016
 */

#pragma once

#include <gtsam/geometry/Pose3.h>
#include <gtsam/geometry/Cal3DS2.h>

#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

#include <boost/tuple/tuple.hpp>

class MonoCalibration {
public:
    /**
     * @brief   Default constructor
     */
    MonoCalibration();

    /**
     * @brief   Constructor
     * @param   nrImages: number of images in the folder
     * @note    Since we cannot extract corners, or obtain satisfactory homographies or initial poses from some images,
     *          the number of initial camera poses will be no greater than the number of
     *          original images in a folder.
     */
    explicit MonoCalibration(std::size_t nrImages) : nrImages_(nrImages) {}

    /**
     * @brief   Estimate homographies
     * @param   corners3D: the corners on the chessboard
     * @param   corners2D: the extracted corners in the images
     * @return  homographies with associated image indices
     */
    std::map<std::size_t, gtsam::Matrix3> estimateHomographies(const std::vector<gtsam::Point2>& corners3D,
                                                              const std::vector<std::vector<gtsam::Point2> >& corners2D);

    /**
     * @brief   Estimate intrinsic matrix with skew included
     * @note    Deprecated
     * @param   homographies: all valid homography matrices
     * @return  3x3 intrinsic matrix
     */
    gtsam::Matrix3 estimateIntrinsics(const std::map<std::size_t, gtsam::Matrix3>& homographies);

    /**
     * @brief   Estimate intrinsic matrix of the camera without skew
     * @param   homographies: all valid homography matrices
     * @return  3x3 intrinsic matrix
     */
    gtsam::Matrix3 estimateIntrinsics2(const std::map<std::size_t, gtsam::Matrix3>& homographies);

    /**
     * @brief   Estimate intrinsic matrix of the camera without skew using vanishing points
     * @cite    http://www.vision.caltech.edu/bouguetj/calib_doc/
     * @param   homographies: all valid homography matrices
     * @return  3x3 intrinsic matrix
     */
    gtsam::Matrix3 estimateIntrinsics2VanishingPoint(const std::map<std::size_t, gtsam::Matrix3>& homographies);

    /**
     * @brief   Estimate camera poses
     * @param   initK: initial estimate of intrinsic matrix
     * @param   homographies: all valid homography matrices
     * @return  all valid camera poses with asscociated image index
     */
    std::map<std::size_t, gtsam::Pose3> estimateExtrinsics(const gtsam::Matrix3& initK,
                                                           const std::map<std::size_t, gtsam::Matrix3>& homographies);

    /**
     * @brief   Calibrate monocular camera
     * @param   initCamPoses: initial estimates of camera poses
     * @param   initK: initial intrinsic matrix
     * @param   corners3D: corners on the calibration rig
     * @param   corners2D: corners in the images
     * @return  a tuple of initial values, resulting values and the factor graph
     */
    boost::tuple<gtsam::Values, gtsam::Values, gtsam::NonlinearFactorGraph>
    calibrateCamera(const std::map<std::size_t, gtsam::Pose3>& initCamPoses,
                    const gtsam::Matrix3& initK, const std::vector<gtsam::Point2>& corners3D,
                    const std::vector<std::vector<gtsam::Point2> >& corners2D);

    /**
     * @brief   Calibrate monocular camera
     * @param   initCamPoses: initial estimates of camera poses
     * @param   initK: initial intrinsic matrix
     * @param   corners3D: corners on the calibration rig
     * @param   corners2D: corners in the images
     * @return  a tuple of initial values, resulting values and the factor graph
     */
    boost::tuple<gtsam::Values, gtsam::Values, gtsam::NonlinearFactorGraph>
    calibrateCamera2(const std::map<std::size_t, gtsam::Pose3>& initCamPoses,
                    const gtsam::Matrix3& initK, const std::vector<gtsam::Point2>& corners3D,
                    const std::vector<std::vector<gtsam::Point2> >& corners2D);

private:
    /**
     * @brief   Normalize the data using a linear transformation T with P3' = T * P3
     * @cite    Copied from Visual Odometry repository by Chris Beall
     * @param   points: s set of 2D points
     * @return  normalized points and the corresponding transformation matrix T
     */
    boost::tuple<std::vector<gtsam::Point2>, gtsam::Matrix3> normalizePoints(const std::vector<gtsam::Point2>& points);

    /**
     * @brief   Estimate a homography given one set of 3D-2D correspondencies
     * @param   points3D: a set of normalized 3D points
     * @param   points2D: a set of normalized 2D points
     * @return  3x3 homography matrix
     */
    gtsam::Matrix3 estimateHomography(const std::vector<gtsam::Point2>& points3D, const std::vector<gtsam::Point2>& points2D);

    /**
     * @brief   Optimize the homography by minimizing the sum of reprojection errors.
     * @note    This method is inspired by the cited reference. It can boost the stability of the whole algorithm.
     * @cite    http://www.vision.caltech.edu/bouguetj/calib_doc/
     * @param   points3D: a set of original (not normalized) 3D points
     * @param   points2D: a set of original (not normalized) 2D points
     * @param   H0: initial guess of the homography from estimateHomography()
     * @return  3x3 homography matrix
     */
    gtsam::Matrix3 optimizeHomography(const std::vector<gtsam::Point2>& points3D, const std::vector<gtsam::Point2>& points2D,
                                     const gtsam::Matrix3& H0);

private:
    std::size_t nrImages_; //< number of images in the folder
};
