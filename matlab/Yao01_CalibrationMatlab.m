clear;
clc;
close all;

%% Define images to process
dir1 = '/home/yao/data/auto-rally-car/platformA_2015-12-06-11-05-26/left_camera/image_color/';
dir2 = '/home/yao/data/auto-rally-car/platformA_2015-12-06-11-05-26/right_camera/image_color/';

imageDir1 = dir(dir1);
imageFileNames1 = cell(size(imageDir1, 1) - 2, 1);
for k = 3:size(imageDir1, 1)
    imageFileNames1(k - 2, 1) = {[dir1 imageDir1(k, 1).name]};
end

imageDir2 = dir(dir2);
imageFileNames2 = cell(size(imageDir2, 1) - 2, 1);
for k = 3:size(imageDir2, 1)
    imageFileNames2(k - 2, 1) = {[dir2 imageDir2(k, 1).name]};
end

%% The code below was automatically generated.
% Detect checkerboards in images
[imagePoints, boardSize, imagesUsed] = detectCheckerboardPoints(imageFileNames1, imageFileNames2);

% Generate world coordinates of the checkerboard keypoints
squareSize = 8.320000e+01;  % in units of 'mm'
worldPoints = generateCheckerboardPoints(boardSize, squareSize);

% Calibrate the camera
[stereoParams, pairsUsed, estimationErrors] = estimateCameraParameters(imagePoints, worldPoints, ...
    'EstimateSkew', true, 'EstimateTangentialDistortion', true, ...
    'NumRadialDistortionCoefficients', 2, 'WorldUnits', 'mm', ...
    'InitialIntrinsicMatrix', [], 'InitialRadialDistortion', []);

% View reprojection errors
h1=figure; showReprojectionErrors(stereoParams, 'BarGraph');

% Visualize pattern locations
h2=figure; showExtrinsics(stereoParams, 'CameraCentric');

% Display parameter estimation errors
displayErrors(estimationErrors, stereoParams);

% You can use the calibration data to rectify stereo images.
I1 = imread(imageFileNames1{1});
I2 = imread(imageFileNames2{1});
[J1, J2] = rectifyStereoImages(I1, I2, stereoParams);
