clear;
clc;
close all;

if exist('diary', 'file')
    delete diary
end
diary 


corners = load('/home/yao/data/autorally/calibration/test_corner/corners2DOpenCV.txt');

filename = 'image.jpg';
image = imread(filename);
% figure(1);
% imshow(image);
% hold on
% for k = 1:40
%     plot(corners(k, 1) + 1, corners(k, 2) + 1, 'Marker', 'x', 'Color', 'r', 'MarkerSize', 6);
% end

I = double(image);
I = 0.299 * I(:,:,1) + 0.5870 * I(:,:,2) + 0.114 * I(:,:,3);
xt = corners';
wintx = 11;
winty = 11;

line_feat = 1; % set to 1 to allow for extraction of line features.

xt = xt';
xt = fliplr(xt);

mask = exp(-((-wintx:wintx)'/(wintx)).^2) * exp(-((-winty:winty)/(winty)).^2);

offx = (-wintx:wintx)' * ones(1, 2*winty+1);
offy = ones(2*wintx+1,1) * (-winty:winty);

resolution = 0.005;

MaxIter = 10;

[nx, ny] = size(I);
N = size(xt,1);

xc = xt; % first guess... they don't move !!!

type = zeros(1,N);


for i=1:N,
    fprintf('Corner: %d\n', i-1);

    v_extra = resolution + 1; 		% just larger than resolution
    compt = 0; 				% no iteration yet

    while (norm(v_extra) > resolution) && (compt<MaxIter)
%         fprintf('Iteration: %d\n', compt);
        cIx = xc(i,1); 			%
        cIy = xc(i,2); 			% Coords. of the point
        crIx = round(cIx); 		% on the initial image
        crIy = round(cIy); 		%
        itIx = cIx - crIx; 		% Coefficients
        itIy = cIy - crIy; 		% to compute
        if itIx > 0, 			% the sub pixel
            vIx = [itIx 1-itIx 0]'; 	% accuracy.
        else
            vIx = [0 1+itIx -itIx]';
        end;
        if itIy > 0,
            vIy = [itIy 1-itIy 0];
        else
            vIy = [0 1+itIy -itIy];
        end;
        


        % What if the sub image is not in?
        if (crIx-wintx-2 < 1), xmin=1; xmax = 2*wintx+5;
        elseif (crIx+wintx+2 > nx), xmax = nx; xmin = nx-2*wintx-4;
        else
            xmin = crIx-wintx-2; xmax = crIx+wintx+2;
        end;

        if (crIy-winty-2 < 1), ymin=1; ymax = 2*winty+5;
        elseif (crIy+winty+2 > ny), ymax = ny; ymin = ny-2*winty-4;
        else
            ymin = crIy-winty-2; ymax = crIy+winty+2;
        end
     


        SI = I((xmin:xmax)+1,(ymin:ymax)+1); % The necessary neighborhood
        SI = conv2(SI,vIx,'same');        
        SI = conv2(SI,vIy,'same');
        SI = SI(2:2*wintx+4,2:2*winty+4); % The subpixel interpolated neighborhood
        
        
        
        
        [gy,gx] = gradient(SI); 		% The gradient image
        gx = gx(2:2*wintx+2,2:2*winty+2); % extraction of the useful parts only
        gy = gy(2:2*wintx+2,2:2*winty+2); % of the gradients
      

        px = cIx + offx;
        py = cIy + offy;
        
        
        

        gxx = gx .* gx .* mask;
        gyy = gy .* gy .* mask;
        gxy = gx .* gy .* mask;


        bb = [sum(sum(gxx .* px + gxy .* py)); sum(sum(gxy .* px + gyy .* py))];

        a = sum(sum(gxx));
        b = sum(sum(gxy));
        c = sum(sum(gyy));

        dt = a*c - b^2;
       
        

        xc2 = [c*bb(1)-b*bb(2) a*bb(2)-b*bb(1)]/dt;
        
        
         

        

            G = [a b;b c];
            [U,S,V]  = svd(G);
             if compt < 20
            fprintf('cx: %4f, crx: %4f\n', cIx, crIx);
            fprintf('cy: %4f, cry: %4f\n', cIy, crIy);
            fprintf('Vx: %4f, %4f, %4f\n', vIx(1), vIx(2), vIx(3));
            fprintf('Vy: %4f, %4f, %4f\n', vIy(1), vIy(2), vIy(3));
            fprintf('xmin: %d, xmax: %d, ymin: %d, ymax: %d\n', xmin, xmax, ymin, ymax);
            if i==1
                Vx = vIx;
                Vy = vIy;
%                 ggx = gx;
%                 ggy = gy;
%                  M = SI;
%                 ppx = px;
%                 ppy = py;
                fprintf('%f, %f, %f\n', a, b, c);
                fprintf('%f, %f\n', xc2(1), xc2(2));
                SS = S;
                VV = V;
            end
        end

            if (S(1,1)/S(2,2) > 50),
                % projection operation:
                xc2 = xc2 + sum((xc(i,:)-xc2).*(V(:,2)'))*V(:,2)';
                type(i) = 1;
            end;

       
        
       
        
        

        if (isnan(xc2(1)) || isnan(xc2(2))),
            xc2 = [0 0];
        end;
        
        v_extra = xc(i,:) - xc2;
        xc(i,:) = xc2;
        compt = compt + 1;

    end
end


% check for points that diverge:

delta_x = xc(:,1) - xt(:,1);
delta_y = xc(:,2) - xt(:,2);

%keyboard;


bad = (abs(delta_x) > wintx) | (abs(delta_y) > winty);
good = ~bad;
in_bad = find(bad);

% For the diverged points, keep the original guesses:

xc(in_bad,:) = xt(in_bad,:);

xc = fliplr(xc);
xc = xc';

bad = bad';
good = good';

% figure(2);
% imshow(image);
% hold on
% for k = 1:40
%     plot(xc(1, k) + 1, xc(2, k) + 1, 'Marker', 'x', 'Color', 'r', 'MarkerSize', 6);
% end
diary off