function [] = renameFiles(inputDir, outputDir)
%renameFiles Summary of this function goes here
%   Detailed explanation goes here
    id = dir([inputDir '*.jpg']);
    for k = 1:size(id, 1)
        src = [inputDir id(k).name];
        dst = [outputDir 'img' sprintf('%06d', k-1) '.jpg'];
        movefile(src, dst);
    end

end

