/**
 * @file    StereoCalibration.h
 * @brief   Calibrate stereo cameras
 * @author  Yao Chen
 * @date    July 18, 2016
 */

#pragma once

#include <gtsam/geometry/Pose3.h>
#include <gtsam/geometry/Cal3DS2.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

#include <boost/tuple/tuple.hpp>

class StereoCalibration {
public:
    /**
     * @brief   default constructor
     */
    StereoCalibration();    

    /**
     * @brief   Construct the class with total number of images
     * @param   nrImages: total number of images in the folder
     * @note    nrImages should be no less than the number of initial poses
     *          because we cannot extract corners in some images or
     *          because we cannot get valid initial pose from one image
     */
    explicit StereoCalibration(std::size_t nrImages) : nrImages_(nrImages) {}

    /**
     * @brief   Create initial values for the factor graph
     * @param   leftResult: calibration result of the left camera
     * @param   rightResult: calibration result of the right camera
     * @return  left camera calibration, right camera calibration, left camera poses and between pose
     */
    boost::tuple<gtsam::Cal3DS2, gtsam::Cal3DS2, std::map<std::size_t, gtsam::Pose3>, gtsam::Pose3>
    init(const gtsam::Values& leftResult, const gtsam::Values& rightResult);


    /**
     * @brief   Estimate extrinsic parameters of the camera
     * @param   K0: initial left camera calibration
     * @param   K1: initial right camera calibration
     * @param   leftCamPoses: initial left camera poses T_L's
     * @param   betweenPose: the pose between left and right cameras T = T_L^{-1}*T_R
     * @return  a tuple of resulting values and the factor graph
     */
    boost::tuple<gtsam::Values, gtsam::NonlinearFactorGraph>
    calibrateCameras(const gtsam::Cal3DS2& K0, const gtsam::Cal3DS2& K1,
                     const std::map<std::size_t, gtsam::Pose3>& leftCamPoses,
                     const gtsam::Pose3& betweenPose,
                     const std::vector<gtsam::Point2>& corners3D,
                     const std::vector<std::vector<gtsam::Point2> >& leftCorners2D,
                     const std::vector<std::vector<gtsam::Point2> >& rightCorners2D);
private:
    std::size_t nrImages_;  //< total number of images in the folder
};
