/**
 * @file    CornerDetector.h
 * @brief   Detect corners from images in one folder.
 * @brief   This is a wrapper of OpenCV's chessboard corner detection with more flexibility.
 * @author  Yao Chen
 * @date    July 18, 2016
 */

#pragma once

#include <opencv2/core/core.hpp>

#include <gtsam/geometry/Point2.h>

class CornerDetector {
public:
    /**
     * @brief   Default constructor
     */
    CornerDetector();

    /**
     * @brief   Constructor with parameters
     * @param   rows: number of inner corners per a chessboard row
     * @param   cols: number of inner corners per a chessboard column
     * @param   nrSkippedImage: number of skipped images
     * @param   nrImages: number of images to be processed
     * @param   imageDir: root directory of the images
     * @param   filenamePre: image filename pattern
     * @param   filenameExt: image type
     * @param   filenameNumWidth: width of numbers after filenamePre
     */
    CornerDetector(int rows, int cols, size_t nrSkippedImage, size_t nrImages,
                   const std::string& imageDir, const std::string& filenamePre,
                   const std::string& filenameExt, int filenameNumWidth) :
        nrSkippedImage_(nrSkippedImage), nrImages_(nrImages), imageDir_(imageDir), filenamePre_(filenamePre),
        filenameExt_(filenameExt), filenameNumWidth_(filenameNumWidth) {
        patternSize_ = cv::Size(rows, cols);
    }

    /**
     * @brief   Destructor
     */
    virtual ~CornerDetector() {}

public:
    /**
     * @brief   Detect corners in all images
     */
    std::vector<std::vector<gtsam::Point2> > detectCorners();

    /**
     * @brief   Visualize the corner detection of a single image
     * @param   corners: corners in one image
     * @param   index: the index of the image
     * @return  True if corners are detected and false otherwise
     */
    bool visualize(const std::vector<gtsam::Point2>& corners, std::size_t index);

    /**
     * @brief   Visualize the corner detection of all images
     * @param   corners: corners in all images
     * @param   pause: the pause time in ms
     * @return  True if corners are detected and false otherwise
     */
    void visualize(const std::vector<std::vector<gtsam::Point2> >& corners, int pause);

    /**
     * @brief   Get the width of the image
     * @return  image width
     */
    int imageWidth() const { return imageWidth_; }

    /**
     * @brief   Get the height of the image
     * @return  image height
     */
    int imageHeight() const { return imageHeight_; }


private:
    /**
     * @brief   Optimize corners into subpixel accuracy. This function implements cornerfinder.m in
     *          Caltech's toolbox as cited below.
     * @cite    http://www.vision.caltech.edu/bouguetj/calib_doc/
     * @param   image: input image (can have either 1 channel or 3 channels)
     * @param   size: size of the mask
     * @param   tol: tolerance to terminate iterations
     * @param   maxIters: maximum number of iterations
     */
    void cornerSubPix2(const cv::Mat& image, std::vector<cv::Point2f>& corners,
                       const cv::Size& size, double tol, int maxIters);

private:

    std::size_t nrSkippedImage_;        //< number of skipped image
    std::size_t nrImages_;              //< number of images

    std::string imageDir_;      //< root directory of the images
    std::string filenamePre_;   //< image filename pattern
    std::string filenameExt_;   //< image type
    int filenameNumWidth_;      //< width of the numbers after filenamePre

    cv::Size patternSize_;      //< pattern size

    int imageWidth_;            //< width of the image
    int imageHeight_;           //< height of the image
};
