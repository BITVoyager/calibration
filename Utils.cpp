/**
 * @file    Utils.h
 * @brief   Utility functions
 * @author  Yao Chen
 * @date    August 16, 2016
 */

#include "Utils.h"

#include <gtsam/geometry/Cal3DS2.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/inference/Symbol.h>

#include <fstream>
#include <iomanip>

using namespace gtsam;
using namespace std;

using symbol_shorthand::B;
using symbol_shorthand::K;

/* Write stereo calibration results into a file */
void writeSetereoCalibrationResults(const string& filename, const gtsam::Values &result) {
    // retrieve all results
    Cal3DS2 KK0 = result.at<Cal3DS2>(K(0));
    Cal3DS2 KK1 = result.at<Cal3DS2>(K(1));
    Pose3 left_P_right = result.at<Pose3>(B(0)).inverse();

    Point3 translation = left_P_right.translation();
    Matrix3 rotation = left_P_right.rotation().matrix();

    ofstream ofs(filename);
    if (ofs.is_open()) {
        ofs << setprecision(10) << scientific;
        ofs << "<param name=\"FC1l\" value=\"" << KK0.fx() << "\"/>" << endl;
        ofs << "<param name=\"FC2l\" value=\"" << KK0.fy() << "\"/>" << endl;
        ofs << "<param name=\"ALPHA_Cl\" value=\"" << KK0.skew() << "\"/>" << endl;
        ofs << "<param name=\"CC1l\" value=\"" << KK0.px() << "\"/>" << endl;
        ofs << "<param name=\"CC2l\" value=\"" << KK0.py() << "\"/>" << endl;
        ofs << "<param name=\"K1l\" value=\"" << KK0.k1() << "\"/>" << endl;
        ofs << "<param name=\"K2l\" value=\"" << KK0.k2() << "\"/>" << endl;
        ofs << "<param name=\"P1l\" value=\"" << KK0.p1() << "\"/>" << endl;
        ofs << "<param name=\"P2l\" value=\"" << KK0.p2() << "\"/>" << endl;
        ofs << endl;

        ofs << "<param name=\"FC1r\" value=\"" << KK1.fx() << "\"/>" << endl;
        ofs << "<param name=\"FC2r\" value=\"" << KK1.fy() << "\"/>" << endl;
        ofs << "<param name=\"ALPHA_Cr\" value=\"" << KK1.skew() << "\"/>" << endl;
        ofs << "<param name=\"CC1r\" value=\"" << KK1.px() << "\"/>" << endl;
        ofs << "<param name=\"CC2r\" value=\"" << KK1.py() << "\"/>" << endl;
        ofs << "<param name=\"K1r\" value=\"" << KK1.k1() << "\"/>" << endl;
        ofs << "<param name=\"K2r\" value=\"" << KK1.k2() << "\"/>" << endl;
        ofs << "<param name=\"P1r\" value=\"" << KK1.p1() << "\"/>" << endl;
        ofs << "<param name=\"P2r\" value=\"" << KK1.p2() << "\"/>" << endl;
        ofs << endl;

        ofs << "<param name=\"T_x\" value=\"" << translation.x() / 1e3 << "\"/>" << endl;
        ofs << "<param name=\"T_y\" value=\"" << translation.y() / 1e3 << "\"/>" << endl;
        ofs << "<param name=\"T_z\" value=\"" << translation.z() / 1e3 << "\"/>" << endl;
        ofs << endl;

        ofs << "<param name=\"R11\" value=\"" << rotation(0, 0) << "\"/>" << endl;
        ofs << "<param name=\"R12\" value=\"" << rotation(0, 1) << "\"/>" << endl;
        ofs << "<param name=\"R13\" value=\"" << rotation(0, 2) << "\"/>" << endl;
        ofs << "<param name=\"R21\" value=\"" << rotation(1, 0) << "\"/>" << endl;
        ofs << "<param name=\"R22\" value=\"" << rotation(1, 1) << "\"/>" << endl;
        ofs << "<param name=\"R23\" value=\"" << rotation(1, 2) << "\"/>" << endl;
        ofs << "<param name=\"R31\" value=\"" << rotation(2, 0) << "\"/>" << endl;
        ofs << "<param name=\"R32\" value=\"" << rotation(2, 1) << "\"/>" << endl;
        ofs << "<param name=\"R33\" value=\"" << rotation(2, 2) << "\"/>" << endl;

        ofs.close();
    } else {
        cout << "Unable to open " << filename.c_str() << endl;
        exit(0);
    }
}

/* Read corners in images from file */
vector<vector<Point2> > readCorners2D(const string& filename, int size) {
    ifstream ifs(filename.c_str());

    vector<vector<Point2> > corners;
    if (ifs.is_open()) {
        int k = 0;

        double x;
        double y;

        vector<Point2> tempCorners;
        while(ifs >> x >> y) {
            tempCorners.push_back(Point2(x, y));
            k++;
            if (k % size == 0) {
                corners.push_back(tempCorners);
                tempCorners.clear();
                k = 0;
            }
        }
    } else {
        cout << "Unable to open " << filename.c_str() << endl;
        exit(0);
    }
    ifs.close();

    return corners;
}

/* Write corners in images to file */
bool writeCorners2D(const string& filename, const vector<vector<Point2> >& corners) {
    ofstream ofs(filename.c_str());

    if (ofs.is_open()) {
       for (size_t i = 0; i < corners.size(); i++) {
           for (size_t j = 0; j < corners[i].size(); j++) {
               ofs << setprecision(10) << corners[i][j].x() << "\t" << corners[i][j].y() << endl;
           }
       }
    } else {
        cout << "Unable to open " << filename.c_str() << endl;
        return false;
    }
    ofs.close();

    return true;
}

/* Read corners on calibration rig from file */
vector<Point2> readCorners3D(const string& filename) {
    ifstream ifs(filename.c_str());

    vector<Point2> corners;
    if (ifs.is_open()) {
        double x;
        double y;
        double z;

        while(ifs >> x >> y >> z) {
            corners.push_back(Point2(x, y));
        }
    } else {
        cout << "Unable to open " << filename.c_str() << endl;
        exit(0);
    }
    ifs.close();
    return corners;
}

bool writeCorners3D(const string& filename, const vector<Point2>& corners) {
    ofstream ofs(filename.c_str());

    if (ofs.is_open()) {
       for (size_t i = 0; i < corners.size(); i++) {
            ofs << corners[i].x() << "\t" << corners[i].y() << "\t" << 0 << endl;
       }
    } else {
        cout << "Unable to open " << filename.c_str() << endl;
        return false;
    }
    ofs.close();

    return true;
}

