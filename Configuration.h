/**
 * @file    Configuration.h
 * @brief   Configuration file for calibration
 * @cite    Visual odometry repository
 * @author  Yao Chen
 * @date    August 16, 2016
 */

#pragma once

#include <string>

class Configuration {
public:
    /**
     * @brief   Default constructor
     */
    Configuration() : rows_(5), cols_(8), nrSkippedImage_(0), nrImages_(50),
        leftImageDir_(""), rightImageDir_(""), filenamePre_("img"), filenameExt_(".jpg"),
        indexPatternWidth_(6), pauseTime_(100), resultFilename_("") {}

    /**
     * @brief   Parse parameters from configuration files (.cfg)
     * @param   filename: filename of the configuration file
     */
    Configuration(const std::string& filename);

public:
    /**
     * @brief   Get the number of rows of the calibration rig
     * @return  The number of rows of the calibration rig
     */
    int rows() const { return rows_; }

    /**
     * @brief   Get the number of columns of the calibration rig
     * @return  The number of columns of the calibration rig
     */
    int cols() const { return cols_; }

    /**
     * @brief   Get the number of skipped images
     * @return  The number of skipped images
     */
    int nrSkippedImage() const { return nrSkippedImage_; }

    /**
     * @brief   Get the number of images in the folder to be processed
     * @return  The number of images in the folder to be processed
     */
    int nrImages() const { return nrImages_; }

    /**
     * @brief   Get the directory of left images
     * @return  Directory of left images
     */
    std::string leftImageDir() const { return leftImageDir_; }

    /**
     * @brief   Get the directory of right images
     * @return  Directory of right images
     */
    std::string rightImageDir() const { return rightImageDir_; }

    /**
     * @brief   Get the prefix of image filename
     * @return  Prefix of image filename
     */
    std::string filenamePre() const { return filenamePre_; }

    /**
     * @brief   Get the extenstion of images
     * @return  Extenstion of images
     */
    std::string filenameExt() const { return filenameExt_; }

    /**
     * @brief   Get the width of index pattern (width of "000201" is 6, width of "000001001" is 9, etc)
     * @return  Width of index pattern
     */
    int indexPatternWidth() const { return indexPatternWidth_; }

    /**
     * @brief   Get the side length of a grid on the calibration rig
     * @return  Side length of a grid on the calibration rig
     */
    double spacing() const { return spacing_; }

    /**
     * @brief   Get the pause time for visualization of corner detection
     * @return  Pause time for visualization of corner detection
     */
    int pauseTime() const { return pauseTime_; }

    /**
     * @brief   Get the output filename used for writing the results
     * @return  Output filename used for writing the results
     */
    std::string resultFilename() const { return resultFilename_; }

private:
    int rows_;
    int cols_;

    int nrSkippedImage_;
    int nrImages_;

    std::string leftImageDir_;
    std::string rightImageDir_;
    std::string filenamePre_;
    std::string filenameExt_;
    int indexPatternWidth_ ;

    double spacing_;

    int pauseTime_;

    std::string resultFilename_;
};
