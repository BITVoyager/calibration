/**
 * @file    Configuration.cpp
 * @brief   Configuration file for calibration
 * @cite    Visual odometry repository
 * @author  Yao Chen
 * @date    August 16, 2016
 */

#include "Configuration.h"

#include <boost/program_options.hpp>

#include <fstream>
#include <iostream>

namespace po = boost::program_options;

using namespace std;

Configuration::Configuration(const string& filename) {
    po::options_description config("Configuration");
    config.add_options()
            ("rows", po::value<int>(&rows_)->default_value(5), "")
            ("cols", po::value<int>(&cols_)->default_value(8), "")
            ("nrSkippedImage", po::value<int>(&nrSkippedImage_)->default_value(0), "")
            ("nrImages", po::value<int>(&nrImages_)->default_value(50), "")
            ("leftImageDir", po::value<string>(&leftImageDir_)->default_value(""), "")
            ("rightImageDir", po::value<string>(&rightImageDir_)->default_value(""), "")
            ("filenamePre",  po::value<string>(&filenamePre_)->default_value("img"), "")
            ("filenameExt",  po::value<string>(&filenameExt_)->default_value(".jpg"), "")
            ("indexPatternWidth", po::value<int>(&indexPatternWidth_)->default_value(6), "")
            ("spacing", po::value<double>(&spacing_)->default_value(83.2), "")
            ("pauseTime", po::value<int>(&pauseTime_)->default_value(100), "")
            ("resultFilename", po::value<string>(&resultFilename_)->default_value(""), "");

    ifstream ifs(filename.c_str());
    po::variables_map vm;
    po::store(po::parse_config_file(ifs, config), vm);
    po::notify(vm);
}
