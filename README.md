# Camera Calibration Toolbox #

This repository deals with monocular and stereo camera calibrations. The pipeline is based on the paper *A flexible new technique for camera calibration* [Zhang00tpami]. The implementation is based on [GTSAM](https://bitbucket.org/gtborg/gtsam), which is an open-source library for modeling and solving smoothing and mapping problems.

### What is this repository for? ###

* Monocular calibration
* Stereo calibration

### Prerequisites ###
* GTSAM  >= 4.0.0
* OpenCV >= 3.2.0
* Boost  >= 1.43

### How do I get set up? ###

```
#!Linux
mkdir build
cmake ..
make check (optional, run unit tests)
make
```

### How do I run the executables? ###
Suppose you are at `/path/to/calibration/build` and you have already specified the hyper parameters in `/path/to/calibration/config/`. You can try:
```
#!Linux
cd examples
./Yao*
```