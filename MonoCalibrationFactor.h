/**
 * @file    MonoCalibrationFactor.h
 * @brief   This factor measures the reprojection error of one single camera.
 *          The camera can be the left, or the right camera for mono calibration, and the left camera in stereo calibration.
 * @author  Yao Chen
 * @date    July 18, 2016
 */

#pragma once

#include <gtsam/geometry/Cal3DS2.h>
#include <gtsam/geometry/PinholeCamera.h>
#include <gtsam/nonlinear/NonlinearFactor.h>

#include <iostream>

class MonoCalibrationFactor : public gtsam::NoiseModelFactor2<gtsam::Pose3, gtsam::Cal3DS2> {
public:
    typedef gtsam::NoiseModelFactor2<gtsam::Pose3, gtsam::Cal3DS2> Base;    //< short hand for base class
    typedef gtsam::PinholeCamera<gtsam::Cal3DS2> Camera;    //< short hand for camera type

public:

    /**
     * @brief   Construct the factor given a point on the calibration rig and its corresponding 2D measurement
     * @param   model: the noise model
     * @param   cameraKey: the key of the camera
     * @param   poseKey: the key of the camera pose
     * @param   P: a 3D point on the calibration rig (Z=0)
     * @param   p: a 2D measurement in the image
     */
    MonoCalibrationFactor(const gtsam::SharedNoiseModel& model, const gtsam::Key& poseKey,
        const gtsam::Key& cameraKey, const gtsam::Point3& P, const gtsam::Point2& p) :
        Base(model, poseKey, cameraKey) , P_(P), p_(p) {}

    /**
     * @brief   Evaluate the projection error
     * @param   calib: the calibration of a camera
     * @param   world_T_camera: pose of the camera
     * @param   H1: optional Jacobian matrix with respect to the pose
     * @param   H2: optional Jacobian matrix with respect to the calibration
     * @return  reprojection error
     */
    gtsam::Vector evaluateError(const gtsam::Pose3& world_T_camera, const gtsam::Cal3DS2& calib, boost::optional<gtsam::Matrix&> H1 =
        boost::none, boost::optional<gtsam::Matrix&> H2 = boost::none) const {
        try {
            Camera camera(world_T_camera, calib);
            return camera.project(P_, H1, boost::none, H2) - p_;
        } catch(gtsam::CheiralityException& e) {
            if (H1) *H1 = gtsam::Matrix26::Zero();
            if (H2) *H2 = gtsam::Matrix29::Zero();
            std::cout << e.what() << ": Landmark is behind Camera " << gtsam::DefaultKeyFormatter(this->key2()) << std::endl;
        }
        return gtsam::Z_2x1;
    }

private:
    gtsam::Point3 P_;  //< 3D point on the calibration rig
    gtsam::Point2 p_;  //< 2D measurement of in the image
};

class MonoCalibrationFactor3 : public gtsam::NoiseModelFactor3<gtsam::Pose3, gtsam::Cal3DS2, gtsam::Point2> {
public:
    typedef gtsam::NoiseModelFactor3<gtsam::Pose3, gtsam::Cal3DS2, gtsam::Point2> Base;    //< short hand for base class
    typedef gtsam::PinholeCamera<gtsam::Cal3DS2> Camera;    //< short hand for camera type

public:

    /**
     * @brief   Construct the factor given a point on the calibration rig
     * @param   model: the noise model
     * @param   cameraKey: the key of the camera
     * @param   poseKey: the key of the camera pose
     * @param   P: a 3D point on the calibration rig (Z=0)
     */
    MonoCalibrationFactor3(const gtsam::SharedNoiseModel& model, const gtsam::Key& poseKey,
        const gtsam::Key& cameraKey, const gtsam::Key& measurementKey, const gtsam::Point3& P) :
        Base(model, poseKey, cameraKey, measurementKey), P_(P) {}

    /**
     * @brief   Evaluate the projection error
     * @param   calib: the calibration of a camera
     * @param   world_T_camera: pose of the camera
     * @param   H1: optional Jacobian matrix with respect to the pose
     * @param   H2: optional Jacobian matrix with respect to the calibration
     * @return  reprojection error
     */
    gtsam::Vector evaluateError(const gtsam::Pose3& world_T_camera, const gtsam::Cal3DS2& calib, const gtsam::Point2& measurement,
                                boost::optional<gtsam::Matrix&> H1 = boost::none, boost::optional<gtsam::Matrix&> H2 = boost::none,
                                boost::optional<gtsam::Matrix&> H3 = boost::none) const {
        try {
            Camera camera(world_T_camera, calib);
            gtsam::Vector error = camera.project(P_, H1, boost::none, H2) - measurement;
            if (H3) *H3 = -gtsam::I_2x2;
            return error;
        } catch(gtsam::CheiralityException& e) {
            if (H1) *H1 = gtsam::Matrix26::Zero();
            if (H2) *H2 = gtsam::Matrix29::Zero();
            if (H3) *H3 = gtsam::Matrix2::Zero();
            std::cout << e.what() << ": Landmark is behind Camera " << gtsam::DefaultKeyFormatter(this->key2()) << std::endl;
        }
        return gtsam::Z_2x1;
    }

private:
    gtsam::Point3 P_;  //< 3D point on the calibration rig
};
