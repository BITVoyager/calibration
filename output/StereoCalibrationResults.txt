<param name="FC1l" value="8.3244021215e+02"/>
<param name="FC2l" value="8.3405102921e+02"/>
<param name="ALPHA_Cl" value="4.6232227103e-01"/>
<param name="CC1l" value="6.3147752122e+02"/>
<param name="CC2l" value="5.0855414874e+02"/>
<param name="K1l" value="-2.1531358006e-01"/>
<param name="K2l" value="8.3696885933e-02"/>
<param name="P1l" value="-1.8511239151e-03"/>
<param name="P2l" value="-8.2389071434e-04"/>

<param name="FC1r" value="8.3638435198e+02"/>
<param name="FC2r" value="8.3855859601e+02"/>
<param name="ALPHA_Cr" value="-7.0393498079e-02"/>
<param name="CC1r" value="6.4879501813e+02"/>
<param name="CC2r" value="5.0791198234e+02"/>
<param name="K1r" value="-2.1514841777e-01"/>
<param name="K2r" value="8.2756416355e-02"/>
<param name="P1r" value="-1.5942837145e-03"/>
<param name="P2r" value="-7.5332186751e-04"/>

<param name="T_x" value="-1.4781774029e-01"/>
<param name="T_y" value="-3.1873558739e-04"/>
<param name="T_z" value="-5.4004719994e-04"/>

<param name="R11" value="9.9997457979e-01"/>
<param name="R12" value="2.3669274744e-03"/>
<param name="R13" value="-6.7258768226e-03"/>
<param name="R21" value="-2.3839765791e-03"/>
<param name="R22" value="9.9999396300e-01"/>
<param name="R23" value="-2.5279667033e-03"/>
<param name="R31" value="6.7198527046e-03"/>
<param name="R32" value="2.5439367747e-03"/>
<param name="R33" value="9.9997418565e-01"/>
