/**
 * @file    testStereoCalibration.cpp
 * @brief   Test methods in the class StereoCalibration
 * @author  Yao Chen
 * @date    July 21, 2016
 */

#include "../StereoCalibration.h"

#include <gtsam/base/Testable.h>
#include <CppUnitLite/TestHarness.h>

#include <gtsam/inference/Symbol.h>

#include <boost/tuple/tuple.hpp>

using namespace gtsam;
using namespace std;

using symbol_shorthand::B;
using symbol_shorthand::K;
using symbol_shorthand::X;

/* Test the method init */
TEST(StereoCalibration, init) {
    // 1. Create the results of the left camera
    Values leftResult;
    leftResult.insert(K(0), Cal3DS2(800, 800, 0, 300, 500, 0, 0));
    leftResult.insert(X(0), Pose3());
    leftResult.insert(X(2), Pose3());
    leftResult.insert(X(3), Pose3());
    leftResult.insert(X(6), Pose3());
    leftResult.insert(X(8), Pose3());

    // 1. Create the results of the right camera
    Values rightResult;
    rightResult.insert(K(0), Cal3DS2(800, 800, 0, 300, 500, 0, 0));
    rightResult.insert(X(0), Pose3());
    rightResult.insert(X(1), Pose3());
    rightResult.insert(X(3), Pose3());
    rightResult.insert(X(7), Pose3());
    rightResult.insert(X(8), Pose3());

    // 3. Obtain the initialization results
    Cal3DS2 K0;
    Cal3DS2 K1;
    map<size_t, Pose3> leftCamPoses;
    Pose3 betweenPose;
    StereoCalibration stereoCalibration(10);
    boost::tie(K0, K1, leftCamPoses, betweenPose) = stereoCalibration.init(leftResult, rightResult);

    // check results
    EXPECT_LONGS_EQUAL(3, leftCamPoses.size());
    EXPECT(leftCamPoses.find(1) == leftCamPoses.end());
    EXPECT(leftCamPoses.find(2) == leftCamPoses.end());
    EXPECT(leftCamPoses.find(6) == leftCamPoses.end());
    EXPECT(leftCamPoses.find(7) == leftCamPoses.end());
    EXPECT(leftCamPoses.find(0) != leftCamPoses.end());
    EXPECT(leftCamPoses.find(3) != leftCamPoses.end());
    EXPECT(leftCamPoses.find(8) != leftCamPoses.end());

}

int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
