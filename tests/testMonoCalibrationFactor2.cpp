/**
 * @file    testMonoCalibrationFactor2.cpp
 * @brief   Test the class MonoCalibrationFactor2
 * @author  Yao Chen
 * @date    July 23, 2016
 */

#include "../MonoCalibrationFactor2.h"

#include <gtsam/base/Testable.h>
#include <CppUnitLite/TestHarness.h>

#include <gtsam/inference/Symbol.h>

using namespace gtsam;
using namespace gtsam::noiseModel;
using namespace std;

using symbol_shorthand::B;
using symbol_shorthand::K;
using symbol_shorthand::X;

/* Test the method evaluateError */
TEST(MonoCalibrationFactor2, evaluateError) {
    // 1. Create a factor
    SharedDiagonal measurementNoise = Diagonal::Sigmas(Vector2(0.5, 0.5));
    Point3 point3D(80.0, 160.0, 0.0);
    Point2 point2D(200, 300);
    MonoCalibrationFactor2 factor(measurementNoise, X(0), B(0), K(0), point3D, point2D);

    // 2. Obtain the error by applying the evaluateError method
    Cal3DS2 calibration(800, 800, 0, 500, 300, 0, 0);
    Matrix4 leftCamPoseM;
    leftCamPoseM << 1.0,  0.0,  0.0, 0.0,
                    0.0, -1.0,  0.0, 0.0,
                    0.0,  0.0, -1.0, 500,
                    0.0,  0.0,  0.0, 1.0;
    Pose3 leftCamPose(leftCamPoseM);

    Matrix4 betweenPoseM;
    betweenPoseM << 0.9962,    0.0,    0.0872,  200,
                       0.0,    1.0,       0.0,  0.0,
                   -0.0872,    0.0,    0.9962,   0.0,
                       0.0,    0.0,       0.0,  1.0;
    Pose3 betweenPose(betweenPoseM);

    Matrix H1;
    Matrix H2;
    Matrix H3;
    Vector error = factor.evaluateError(leftCamPose, betweenPose, calibration, H1, H2, H3);

    // 3. Check the error
    EXPECT_LONGS_EQUAL(2, error.rows());
    Matrix poseM = leftCamPoseM * betweenPoseM;
    Matrix34 M;
    M << 1, 0, 0, 0,
         0, 1, 0, 0,
         0, 0, 1, 0;
    Vector point = calibration.K() * M * poseM.inverse() * Vector4(80.0, 160.0, 0.0, 1.0);
    EXPECT_DOUBLES_EQUAL(point(0) / point(2) - 200, error(0), 1e-9);
    EXPECT_DOUBLES_EQUAL(point(1) / point(2) - 300, error(1), 1e-2);
    /* The y component has some large numerical error. TODO: figure out why */
}

int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
