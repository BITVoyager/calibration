/**
 * @file    testMonoCalibration.cpp
 * @brief   Test methods in the class MonoCalibration
 * @author  Yao Chen
 * @date    July 21, 2016
 */

#include "../MonoCalibration.h"

#include <gtsam/base/Testable.h>
#include <CppUnitLite/TestHarness.h>

#include <gtsam/geometry/Cal3_S2.h>
#include <gtsam/geometry/Cal3DS2.h>
#include <gtsam/geometry/PinholeCamera.h>

#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace gtsam;

/* generate corners on the calibration rig */
vector<Point2> generateCorners3D(int rows, int cols, double spacing) {
    vector<Point2> corners3D;
    for (int y = cols - 1; y >= 0; y--) {
        for (int x = 0; x < rows; x++) {
            double px = spacing * static_cast<double>(x);
            double py = spacing * static_cast<double>(y);
            corners3D.push_back(Point2(px, py));
        }
    }
    return corners3D;
}

/* generate a map of 6 homographies */
map<size_t, Matrix> generateHomographies() {
    map<size_t, Matrix> homographies;
    Matrix3 H0;
    H0 << -0.181188,     0.909251,      384.037,
           0.609087,   -0.0723162,      311.508,
       -0.000427829,  3.81029e-05,            1;
    homographies.emplace(0, H0);

    Matrix3 H1;
    H1 << -0.276451,      0.85973,      409.514,
           0.463763,   -0.0653853,      150.637,
       -0.000554645,  4.07107e-05,            1;
    homographies.emplace(1, H1);

    Matrix3 H2;
    H2 << 0.423211,     1.21005,     343.856,
            1.2832,  -0.0456564,     179.003,
       0.000568393, 0.000113204,           1;
    homographies.emplace(2, H2);

    Matrix3 H3;
    H3 << 0.423211,     1.21005,     343.856,
            1.2832,  -0.0456564,     179.003,
       0.000568393, 0.000113204,           1;
    homographies.emplace(3, H3);

    Matrix3 H4;
    H4 <<  0.423264,     1.15244,     296.404,
            1.29689,  -0.0738334,     289.839,
        0.000548935, 4.24169e-05,           1;
    homographies.emplace(4, H4);

    Matrix3 H5;
    H5 << 0.00359482,     0.785148,      328.871,
            0.675418,    -0.072643,      174.462,
        -0.000161987,  5.45464e-05,            1;
    homographies.emplace(5, H5);

    return homographies;
}

/* Test the method estimateHomographies */
TEST(MonoCalibration, estimateHomographies) {
    // 1. Create a camera without distortions
    // We only want to test the homography estimation so that we can just ignore the distortions.
    // Also, distortions are not considered in homography estimation.
    Cal3_S2 calibration(830, 830, 0.0, 640, 512);
    Matrix3 rotM;
    rotM << 0.0995995,   0.922368,  -0.373252,
             0.994686,   -0.10213,  0.0130442,
           -0.0260886,  -0.372567,  -0.927638;
    Point3 translation(570, 240, 760);
    Pose3 camPose(Rot3(rotM), translation);
    PinholeCamera<Cal3_S2> camera(camPose, calibration);

    // 2. Create 3D corners on the calibration rig
    int rows = 5;
    int cols = 8;
    double spacing = 80.0;
    vector<Point2> corners3D = generateCorners3D(rows, cols, spacing);

    // 3. Project those corners in the image (ground truth)
    vector<vector<Point2> > corners2D;
    vector<Point2> tempCorners2D;
    for (const Point2& corner3D: corners3D) {
        Point2 corner = camera.project(Point3(corner3D.x(), corner3D.y(), 0.0));
        EXPECT(corner.x() >= 0 && corner.x() < 1280);
        EXPECT(corner.y() >= 0 && corner.y() < 1024);
        tempCorners2D.push_back(corner);
    }
    corners2D.push_back(tempCorners2D);

    // 4. Obtain the homography from the function
    MonoCalibration monoCalibration(1);
    map<size_t, Matrix> result = monoCalibration.estimateHomographies(corners3D, corners2D);
    EXPECT_LONGS_EQUAL(1, result.size());
    Matrix3 H0 = result.at(0);

    // 5. Use the estimated homography to "project" a 3D corner in the image and
    // measure the error between the result and the previouly-calculated 2D corner.
    size_t k = 0;
    for (const Point2& corner3D: corners3D) {
        Point3 corner(corner3D.x(), corner3D.y(), 1.0);
        Point3 temp = H0 * corner;
        Point2 corner2D(temp.x() / temp.z(), temp.y() / temp.z());
        EXPECT(corner2D.equals(tempCorners2D[k], 1e-8));
        k++;
    }
}

/* Test the method estimateIntrinsics */
TEST(MonoCalibration, estimateIntrinsics) {
    // 1. Create several homographies
    map<size_t, Matrix> homographies = generateHomographies();

    // 2. Estimate intrinsics
    MonoCalibration monoCalibration(6);
    Matrix3 K = monoCalibration.estimateIntrinsics(homographies);

    // 3. Evaluate the constraints
    // The intrinsics are calculated based on *two* constraints:
    // r1 and r2 are orthonormal, where r1 and r2 are the first and second columns of the rotation matrix, respectively.
    Matrix3 Kinv = K.inverse();
    Matrix3 B = Kinv.transpose() * Kinv;
    for (const auto& entry: homographies) {
        Matrix3 H = entry.second;
        Vector3 h1 = H.col(0);
        Vector3 h2 = H.col(1);

        EXPECT_DOUBLES_EQUAL(0.0, h1.transpose() * B * h2, 1e-7);
        EXPECT_DOUBLES_EQUAL(h1.transpose() * B * h1, h2.transpose() * B * h2, 1e-7);
    }
}

/* Test the method estimateIntrinsics2 */
TEST(MonoCalibration, estimateIntrinsics2) {
    // 1. Create several homographies
     map<size_t, Matrix> homographies = generateHomographies();

    // 2. Estimate intrinsics
    MonoCalibration monoCalibration(6);
    Matrix3 K = monoCalibration.estimateIntrinsics2(homographies);

    // 3. Evaluate the constraints
    // The intrinsics are calculated based on *two* constraints:
    // r1 and r2 are orthonormal, where r1 and r2 are the first and second columns of the rotation matrix, respectively.
    Matrix3 Kinv = K.inverse();
    Matrix3 B = Kinv.transpose() * Kinv;
    for (const auto& entry: homographies) {
        Matrix3 H = entry.second;
        Vector3 h1 = H.col(0);
        Vector3 h2 = H.col(1);

        EXPECT_DOUBLES_EQUAL(0.0, h1.transpose() * B * h2, 1e-7);
        EXPECT_DOUBLES_EQUAL(h1.transpose() * B * h1, h2.transpose() * B * h2, 1e-7);
    }
}

/* Test the method estimateExtrinsics */
TEST(MonoCalibration, estimateExtrinsics) {
    // 1. Create several homographies
     map<size_t, Matrix> homographies = generateHomographies();

    // 2. Estimate intrinsics
    MonoCalibration monoCalibration(6);
    Matrix3 K = monoCalibration.estimateIntrinsics2(homographies);

    // 3. Estimate extrinsics
    map<size_t, Pose3> poses = monoCalibration.estimateExtrinsics(K, homographies);
    for (const auto& entry: poses) {
        // Check the rotation matrix
        Pose3 pose = entry.second;
        Matrix3 rotM = pose.rotation().matrix();
        Matrix3 rotM4Test = rotM * rotM.transpose();
        Rot3 rot4Test(rotM4Test);
        EXPECT(rot4Test.equals(Rot3(), 1e-8));
    }
}

int main() {
    TestResult tr;
    return TestRegistry::runAllTests(tr);
}
