/**
 * @file    testMonoCalibrationFactor.cpp
 * @brief   Test the class MonoCalibrationFactor
 * @author  Yao Chen
 * @date    July 23, 2016
 */

#include "../MonoCalibrationFactor.h"

#include <gtsam/base/Testable.h>
#include <CppUnitLite/TestHarness.h>

#include <gtsam/inference/Symbol.h>

using namespace gtsam;
using namespace gtsam::noiseModel;
using namespace std;

using symbol_shorthand::K;
using symbol_shorthand::X;

/* Test the method evaluateError */
TEST(MonoCalibrationFactor, evaluateError) {
    // 1. Create a factor
    SharedDiagonal measurementNoise = Diagonal::Sigmas(Vector2(0.5, 0.5));
    Point3 point3D(80.0, 160.0, 0.0);
    Point2 point2D(200, 300);
    MonoCalibrationFactor factor(measurementNoise, X(0), K(0), point3D, point2D);

    // 2. Obtain the error by applying the evaluateError method
    Cal3DS2 calibration(800, 800, 0, 500, 300, 0, 0);
    Matrix4 camPoseM;
    camPoseM << 1.0,  0.0,  0.0, 0.0,
                0.0, -1.0,  0.0, 0.0,
                0.0,  0.0, -1.0, 500,
                0.0,  0.0,  0.0, 1.0;
    Pose3 camPose(camPoseM);
    Matrix H1;
    Matrix H2;
    Vector error = factor.evaluateError(camPose, calibration, H1, H2);

    // 3. Check the error
    EXPECT_LONGS_EQUAL(2, error.rows());
    EXPECT_DOUBLES_EQUAL(428.0, error(0), 1e-9);
    EXPECT_DOUBLES_EQUAL(-256.0, error(1), 1e-9);
}

int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
