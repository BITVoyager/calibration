/**
 * @file    MonoCalibrationFactor2.h
 * @brief   This factor measures the reprojection error of the right camera in stereo calibration.
 *          The pose of the right camera is given by T_R = T_L * T_B, where T_L is the pose of the left camera
 *          and T_B is the transform of the right camera with respect to the left camera (between pose).
 * @author  Yao Chen
 * @date    July 18, 2016
 */

#pragma once

#include <gtsam/geometry/Cal3DS2.h>
#include <gtsam/geometry/PinholeCamera.h>
#include <gtsam/nonlinear/NonlinearFactor.h>

#include <iostream>

class MonoCalibrationFactor2 : public gtsam::NoiseModelFactor3<gtsam::Pose3, gtsam::Pose3, gtsam::Cal3DS2> {
public:
    typedef gtsam::NoiseModelFactor3<gtsam::Pose3, gtsam::Pose3, gtsam::Cal3DS2> Base;  //< short hand base type
    typedef gtsam::PinholeCamera<gtsam::Cal3DS2> Camera;    // short hand camera type

public:

    /**
     * @brief   Construct the factor given a point on the calibration rig and its corresponding 2D measurement in the image
     * @param   model: the noise model
     * @param   leftCamPoseKey: the key of left camera pose
     * @param   betweenPoseKey: the key of between pose
     * @param   rightCalibKey: the key of right camera calibration
     * @param   P: a 3D point in chessboard frame
     * @param   p: a 2D measurement in right image
     */
    MonoCalibrationFactor2(const gtsam::SharedNoiseModel& model, const gtsam::Key& leftCamPoseKey, const gtsam::Key& betweenPoseKey,
        const gtsam::Key& rightCalibKey, const gtsam::Point3& P, const gtsam::Point2& p) :
        Base(model, leftCamPoseKey, betweenPoseKey, rightCalibKey) , P_(P), p_(p) {}

    /**
     * @brief   Evaluate the reprojection error
     * @param   world_T_left: the pose of the left camera
     * @param   left_T_right: the transformation ("between" pose) of the right camera with respect to the left camera
     *          P_L = T_B * P_R, where P_L is a point in left camera frame and P_R is one in right camera frame.
     * @param   rightCalib: the calibration of the right camera
     * @param   H1: Jacobian matrix of the error with respect to the leftCamPose
     * @param   H2: Jacobian matrix of the error with respect to the betweenPose
     * @param   H3: Jacobian matrix of the error with respect to the rightCalib
     * @return  reprojection error
     */
    gtsam::Vector evaluateError(const gtsam::Pose3& world_T_left, const gtsam::Pose3& left_T_right, const gtsam::Cal3DS2& rightCalib,
                                boost::optional<gtsam::Matrix&> H1 = boost::none, boost::optional<gtsam::Matrix&> H2 = boost::none,
                                boost::optional<gtsam::Matrix&> H3 = boost::none) const {
        try {
            // create the left camera
            gtsam::Matrix6 right_H_left;
            gtsam::Matrix6 right_H_right;
            const gtsam::Pose3 world_T_right = world_T_left.compose(left_T_right, right_H_left, right_H_right);
            Camera rightCamera(world_T_right, rightCalib);

            // project the 3D point onto the image
            gtsam::Matrix26 error_H_right;   // Jacobian of the error with respect to right camera pose
            const gtsam::Vector error = rightCamera.project(P_, error_H_right, boost::none, H3) - p_;

            if (H1) *H1 = error_H_right * right_H_left;
            if (H2) *H2 = error_H_right * right_H_right;

            return error;

        } catch(gtsam::CheiralityException& e) {
            if (H1) *H1 = gtsam::Matrix26::Zero();
            if (H2) *H2 = gtsam::Matrix26::Zero();
            if (H3) *H3 = gtsam::Matrix29::Zero();

            std::cout << e.what() << ": Landmark is behind Camera " << gtsam::DefaultKeyFormatter(this->key3()) << std::endl;
        }
        return gtsam::Z_2x1;
    }

private:
    gtsam::Point3 P_;  //< 3D point on the calibration rig
    gtsam::Point2 p_;  //< 2D measurement of the 3D point in the right image
};
