/**
 * @file    CornerDetector.cpp
 * @brief   Detect corners from images in one folder.
 * @author  Yao Chen
 * @date    July 18, 2016
 */

#include "CornerDetector.h"
#include "Utils.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <boost/tuple/tuple.hpp>

#include <sstream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <fstream>
#include <stdio.h>

using namespace std;
using namespace gtsam;

/* Create image filename */
string createImageFilename(size_t k, string imageDir, string filenamePre, string filenameExt, int filenameNumWidth) {
    ostringstream ss;
    ss << setw(filenameNumWidth) << setfill('0') << k;
    const string filename = imageDir + filenamePre + ss.str() + filenameExt;
    return filename;
}

boost::tuple<cv::Mat, cv::Mat> gradient(const cv::Mat& M) {
    assert(M.rows > 5 && M.cols > 5);

    cv::Mat gx1 = M.col(1) - M.col(0);
    cv::Mat gx2 = 0.5 * M(cv::Rect(2, 0, M.cols - 2, M.rows)) - 0.5 * M(cv::Rect(0, 0, M.cols - 2, M.rows));
    cv::Mat gx3 = M.col(M.cols - 1) - M.col(M.cols - 2);
    vector<cv::Mat> gxs = { gx1, gx2, gx3, };
    cv::Mat gx;
    cv::hconcat(gxs, gx);

    cv::Mat gy1 = M.row(1) - M.row(0);
    cv::Mat gy2 = 0.5 * M(cv::Rect(0, 2, M.cols, M.rows - 2)) - 0.5 * M(cv::Rect(0, 0, M.cols, M.rows - 2));
    cv::Mat gy3 = M.row(M.rows - 1) - M.row(M.rows - 2);
    vector<cv::Mat> gys = { gy1, gy2, gy3, };
    cv::Mat gy;
    cv::vconcat(gys, gy);

    return boost::make_tuple(gx, gy);
}

/* Optimize corners into sub-pixel accuracy */
void CornerDetector::cornerSubPix2(const cv::Mat& originalImage, vector<cv::Point2f>& corners,
                                   const cv::Size& size, double tol, int maxIters) {
    /// optional conversion from color image to gray-scale image
    cv::Mat tempImage;
    originalImage.convertTo(tempImage, CV_64F);
    vector<cv::Mat> channels;
    cv::split(tempImage, channels);
    cv::Mat image;
    image = 0.299 * channels[2] + 0.5870 * channels[1] + 0.114 * channels[0];

    /// half size of the searching kernel
    int wintx = size.width;
    int winty = size.height;

    /// prepare a Gaussian kernel
    cv::Mat maskX(2*wintx+1, 1, CV_64F);
    for (int r = 0; r < maskX.rows; r++) {
        double rr = static_cast<double>(r - wintx) / static_cast<double>(wintx);
        maskX.at<double>(r, 0) = exp(-rr * rr);
    }
    cv::Mat maskY(1, 2*winty+1, CV_64F);
    for (int c = 0; c < maskY.cols; c++) {
        double cc = static_cast<double>(c - winty) / static_cast<double>(winty);
        maskY.at<double>(0, c) = exp(-cc * cc);
    }
    cv::Mat mask = maskX * maskY;

    /// point offsets in X and Y
    cv::Mat offx(2*wintx+1, 2*wintx+1, CV_64F);
    for (int k = -wintx; k <= wintx; k++) {
        auto row = offx.row(k + wintx);
        row.setTo(cv::Scalar(static_cast<double>(k)));
    }
    cv::Mat offy(2*winty+1, 2*winty+1, CV_64F);
    for (int k = -winty; k <= winty; k++) {
        auto row = offy.row(k + winty);
        row.setTo(cv::Scalar(static_cast<double>(k)));
    }

    /// image geometry
    for (size_t k = 0; k < corners.size(); k++) {
        double cx = corners[k].x;
        double cy = corners[k].y;

        int iter = 0;
        double error = 1e10;
        while (error > tol && iter < maxIters) {
            double crx = round(cx);
            double dx = cx - crx;
            cv::Mat kernelX(1, 3, CV_64F);
            if (dx > 0.0) {
                kernelX.at<double>(0, 2) = dx;
                kernelX.at<double>(0, 1) = 1.0 - dx;
                kernelX.at<double>(0, 0) = 0.0;
            } else {
                kernelX.at<double>(0, 2) = 0.0;
                kernelX.at<double>(0, 1) = 1.0 + dx;
                kernelX.at<double>(0, 0) = -dx;
            }

            double cry = round(cy);
            double dy = cy - cry;
            cv::Mat kernelY(3, 1, CV_64F);
            if (dy > 0.0) {
                kernelY.at<double>(2, 0) = dy;
                kernelY.at<double>(1, 0) = 1.0 - dy;
                kernelY.at<double>(0, 0) = 0.0;
            } else {
                kernelY.at<double>(2, 0) = 0.0;
                kernelY.at<double>(1, 0) = 1.0 + dy;
                kernelY.at<double>(0, 0) = -dy;
            }

            int xmin = 0;
            int xmax = 0;
            if (static_cast<int>(crx) - wintx - 2 < 0) {
                xmin = 0;
                xmax = 2 * wintx + 4;
            } else if (static_cast<int>(crx) + wintx + 2 > imageWidth_ - 1) {
                xmin = imageWidth_ - 2 * wintx - 6;
                xmax = imageWidth_ - 1;
            } else {
                xmin = static_cast<int>(crx) - wintx - 2;
                xmax = static_cast<int>(crx) + wintx + 2;
            }

            int ymin = 0;
            int ymax = 0;
            if (static_cast<int>(cry) - winty - 2 < 0) {
                ymin = 0;
                ymax = 2 * winty + 4;
            } else if (static_cast<int>(cry) + winty + 2 > imageHeight_ - 1) {
                ymin = imageHeight_ - 2 * winty - 6;
                ymax = imageHeight_ - 1;
            } else {
                ymin = static_cast<int>(cry) - winty - 2;
                ymax = static_cast<int>(cry) + winty + 2;
            }

            cv::Mat SI = image(cv::Rect(xmin, ymin, xmax - xmin + 1, ymax - ymin + 1));
            cv::filter2D(SI, SI, -1, kernelY);
            cv::filter2D(SI, SI, -1, kernelX);
            SI = SI(cv::Rect(1, 1, 2 * wintx + 3, 2 * winty + 3));

            cv::Mat gx;
            cv::Mat gy;
            boost::tie(gx, gy) = gradient(SI);
            gx = gx(cv::Rect(1, 1, 2*wintx+1, 2*winty+1));
            gy = gy(cv::Rect(1, 1, 2*wintx+1, 2*winty+1));

            cv::Mat px = offx + cx;
            cv::Mat py = offy + cy;

            cv::Mat gxx = gx.mul(gx).mul(mask);
            cv::Mat gyy = gy.mul(gy).mul(mask);
            cv::Mat gxy = gx.mul(gy).mul(mask);

            double bb1 = cv::sum(gxx.mul(px) + gxy.mul(py))[0];
            double bb2 = cv::sum(gxy.mul(px) + gyy.mul(py))[0];
            double a = cv::sum(gxx)[0];
            double b = cv::sum(gxy)[0];
            double c = cv::sum(gyy)[0];
            double dt = a * c - b * b;

            double ccx = (c * bb1 - b * bb2) / dt;
            double ccy = (a * bb2 - b * bb1) / dt;

            cv::Mat G = (cv::Mat_<double>(2, 2) << c, b, b, a);
            cv::Mat W, U, VT;
            cv::SVD::compute(G, W, U, VT, cv::SVD::FULL_UV);

            if (W.at<double>(0, 0) / W.at<double>(1, 0) > 50.0) {
                cout << "!!!" << endl;
                cv::Mat A = (cv::Mat_<double>(1, 2) << cy-ccy, cx-ccx);
                cv::Mat v = VT.row(1);
                double tt = cv::sum(A.mul(v))[0];
                ccx += tt * v.at<double>(0, 1);
                ccy += tt * v.at<double>(0, 0);
            }

            if (fabs(dt) < 1e-5) {
                ccx = 0.0;
                ccy = 0.0;
            }

            error = sqrt((cx-ccx)*(cx-ccx) + (cy-ccy)*(cy-ccy));

            printf("Corner: %ld\n", k);
            printf("Iteration: %d\n", iter);
            printf("cx: %4f, crx: %4f\n", cx, crx);
            printf("cy: %4f, cry: %4f\n", cy, cry);
            printf("Vx: %4f, %4f, %4f\n", kernelY.at<double>(0, 0), kernelY.at<double>(1, 0), kernelY.at<double>(2, 0));
            printf("Vy: %4f, %4f, %4f\n", kernelX.at<double>(0, 0), kernelX.at<double>(0, 1), kernelX.at<double>(0, 2));
            printf("xmin: %d, xmax: %d, ymin: %d, ymax: %d\n", xmin, xmax, ymin, ymax);
            printf("a: %f, b: %f, c:%f\n", a, b, c);
            printf("ccx: %f, ccy: %f\n", ccx, ccy);
            printf("Error: %f\n", error);

            cx = ccx;
            cy = ccy;
            iter++;
        }
        corners[k].x = cx;
        corners[k].y = cy;
    }
}

/* Detect corners from images in a folder */
vector<vector<Point2> > CornerDetector::detectCorners() {
    // define the corners in OpenCV's convention
    // We keep this because it can be used to visualize the results.
    vector<vector<cv::Point2f> > cornersOpenCV;

    // loop over all images
    for (size_t k = nrSkippedImage_; k < nrSkippedImage_ + nrImages_; k++) {

        // construct the filename
        const string filename = createImageFilename(k, imageDir_, filenamePre_, filenameExt_, filenameNumWidth_);
        cv::Mat originalImage = cv::imread(filename, -1);

        // convert it to gray-scale if needed
        cv::Mat image;
        if (originalImage.channels() == 3) {
            cv::cvtColor(originalImage, image, CV_RGB2GRAY);
        } else if (image.channels() != 1) {
            throw runtime_error("[Corner Detector] Wrong number of channels!");
            exit(-1);
        }

        // get the size of the image
        imageWidth_ = image.cols;
        imageHeight_ = image.rows;

        // find chess board corners
        vector<cv::Point2f> tempCorners;
        bool patternFound = cv::findChessboardCorners(image, patternSize_, tempCorners,
                                  cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE + cv::CALIB_CB_FAST_CHECK);

        // If the chess board was detected, find out the sub pixel approximation.
        if (patternFound) {
            // find the most robust chessboard frame
            size_t originIdx1 = size_t(patternSize_.area() - patternSize_.height);
            size_t originIdx2 = size_t(patternSize_.height - 1);
            double dist1 = cv::norm(tempCorners[originIdx1]);
            double dist2 = cv::norm(tempCorners[originIdx2]);
            if (dist1 > dist2) {
                reverse(tempCorners.begin(), tempCorners.end());
            }
            const string ffname = "corners2DOpenCV.txt";
            ofstream ofs(ffname.c_str());

            if (ofs.is_open()) {
               ofs << setprecision(7);
               for (size_t j = 0; j < tempCorners.size(); j++) {

                   ofs << tempCorners[j].x << "\t" << tempCorners[j].y << endl;
               }

            } else {
                cout << "Unable to open " << filename.c_str() << endl;
                exit(-1);
            }
            ofs.close();

           cv::cornerSubPix(image, tempCorners, cv::Size(11, 11), cv::Size(-1, -1),
                            cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.005));
            // cornerSubPix(originalImage, tempCorners, cv::Size(11, 11), 0.005, 300);
        } else {
            tempCorners.clear();
        }

        // push the corners into the vector
        // If no corners are detected, an empty corner vector associated with one image will be pushed.
        cornersOpenCV.push_back(tempCorners);
    }

    // create the output
    vector<vector<gtsam::Point2> > cornersGTSAM;
    for (size_t i = 0; i < cornersOpenCV.size(); i++) {
        // process the corners in one image
        vector<Point2> tempCorners;
        for (size_t j = 0; j < cornersOpenCV[i].size(); j++) {
            Point2 corner(cornersOpenCV[i][j].x, cornersOpenCV[i][j].y);
            tempCorners.push_back(corner);
        }
        cornersGTSAM.push_back(tempCorners);
    }

    return cornersGTSAM;
}

/* Visualize the corner detection of a single image */
bool CornerDetector::visualize(const vector<Point2>& corners, size_t index) {
    // create the image name
    string filename = createImageFilename(index + nrSkippedImage_, "", filenamePre_, filenameExt_, filenameNumWidth_);

    if (corners.size() == 0) {
        cout << "[CornerDetector] No corners are found in " << filename.c_str() << endl;
        return false;
    } else {
        cout << "[CornerDetector] " << corners.size() << " corners are found in " << filename.c_str() << endl;

        string fn = imageDir_ + filename;
        cv::Mat image = cv::imread(fn, -1);
        if (image.channels() == 1) {
            cv::cvtColor(image, image, CV_GRAY2RGB);
        }

        for (size_t i = 0; i < corners.size(); i++) {
            cv::putText(image, to_string(i), cv::Point(corners[i].x(), corners[i].y()),
                        cv::FONT_HERSHEY_SIMPLEX, 0.3, CV_RGB(255, 0, 0));
        }
        cv::putText(image, filename, cv::Point(imageWidth() - 200, imageHeight() - 30),
                    cv::FONT_HERSHEY_SIMPLEX, 0.5, CV_RGB(255, 0, 0));

        string windowName = "Corners in Image";
        cv::imshow(windowName, image);

        return true;
    }
}

/* Visualize the corner detection of all image */
void CornerDetector::visualize(const vector<vector<Point2> >& corners, int pause) {
    vector<size_t> invalidList;
    for (size_t k = 0; k < corners.size(); k++) {
        if (!visualize(corners[k], k)) {
            invalidList.push_back(k);
        }
        cvWaitKey(pause);
    }

    cout << "[CornerDetector] Summary: " << endl;
    cout << "Number of loaded images: " << corners.size() << endl;
    cout << "Number of valid images: " <<  corners.size() - invalidList.size() << endl;
    cout << "Invalid images: ";
    for (size_t i: invalidList) {
        cout << i + nrSkippedImage_ << " ";
    }
    cout << endl;

}
