/**
 * @file    Yao03_StereoCalibration.cpp
 * @brief   Example of stereo calibration
 * @author  Yao Chen
 * @date    July 19, 2016
 */

#include "../Configuration.h"
#include "../CornerDetector.h"
#include "../MonoCalibration.h"
#include "../StereoCalibration.h"
#include "../Utils.h"

#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/Marginals.h>

#include <boost/lexical_cast.hpp>

#include <iostream>

using namespace std;
using namespace gtsam;

using symbol_shorthand::B;
using symbol_shorthand::K;
using symbol_shorthand::X;
using symbol_shorthand::Z;

Values calibrateMonoCamera(const vector<Point2>& corners3D, const vector<vector<Point2> >& corners2D) {
    MonoCalibration monoCalibration(corners2D.size());
    map<size_t, Matrix3> homographies = monoCalibration.estimateHomographies(corners3D, corners2D);
    Matrix3 initK = monoCalibration.estimateIntrinsics2VanishingPoint(homographies);
    map<size_t, Pose3> initCamPoses = monoCalibration.estimateExtrinsics(initK, homographies);
    Values initial;
    Values result;
    NonlinearFactorGraph graph;
    boost::tie(initial, result, graph) = monoCalibration.calibrateCamera2(initCamPoses, initK, corners3D, corners2D);
    return result;
}

int main(int argc, const char* argv[]) {
    /// parse arguments
    const string configDir = "../config/";
    string configFilename = configDir + "platformA_2015-12-06-11-05-26.cfg";
    if (argc == 2) {
       configFilename = configDir + boost::lexical_cast<string>(argv[1]);
    }
    Configuration config(configFilename);

    /// set parameters
    int rows = config.rows();
    int cols = config.cols();
    int nrSkippedImage = config.nrSkippedImage();
    int nrImages = config.nrImages();
    const string leftImageDir = config.leftImageDir();
    const string rightImageDir = config.rightImageDir();
    const string filenamePre = config.filenamePre();
    const string filenameExt = config.filenameExt();
    int filenameNumWidth = config.indexPatternWidth();
    double spacing = config.spacing();
    int pauseTime = config.pauseTime();
    string resultFilename = config.resultFilename();

    cout << "[Configuration] rows: " << rows << endl;
    cout << "[Configuration] cols: " << cols << endl;
    cout << "[Configuration] nrSkippedImage: " << nrSkippedImage << endl;
    cout << "[Configuration] nrImages: " << nrImages << endl;
    cout << "[Configuration] leftImageDir: " << leftImageDir.c_str() << endl;
    cout << "[Configuration] rightImageDir: " << rightImageDir.c_str() << endl;
    cout << "[Configuration] filenamePre: " << filenamePre.c_str() << endl;
    cout << "[Configuration] filenameExt: " << filenameExt.c_str() << endl;
    cout << "[Configuration] filenameNumWidth: " << filenameNumWidth << endl;
    cout << "[Configuration] spacing: " << spacing << endl;
    cout << "[Configuration] pauseTime: " << pauseTime << endl;
    cout << "[Configuration] resultFilename: " << resultFilename << endl;
    cout << endl;

    /// detect corners
    cout << "Extracting corners from left images..." << endl;
    CornerDetector leftCornerDetector(rows, cols, nrSkippedImage, nrImages, leftImageDir, filenamePre, filenameExt, filenameNumWidth);
    vector<vector<Point2> > leftCorners2D = leftCornerDetector.detectCorners();
    leftCornerDetector.visualize(leftCorners2D, pauseTime);
    cout << "Done extracting corners from left images." << endl;
    cout << endl;

    cout << "Extracting corners from right images..." << endl;
    CornerDetector rightCornerDetector(rows, cols, nrSkippedImage, nrImages, rightImageDir, filenamePre, filenameExt, filenameNumWidth);
    vector<vector<Point2> > rightCorners2D = rightCornerDetector.detectCorners();
    rightCornerDetector.visualize(rightCorners2D, pauseTime);
    cout << "Done extracting corners from right images." << endl;
    cout << endl;

    /// create corners on the calibration rig
    vector<Point2> corners3D;
    for (int y = cols - 1; y >= 0; y--) {
        for (int x = 0; x < rows; x++) {
            double px = spacing * static_cast<double>(x);
            double py = spacing * static_cast<double>(y);
            corners3D.push_back(Point2(px, py));
        }
    }

    /// monocular calibrations
    assert(nrImages == leftCorners2D.size());
    assert(nrImages == rightCorners2D.size());
    cout << "Calibrating left camera..." << endl;
    Values leftResult = calibrateMonoCamera(corners3D, leftCorners2D);
    cout << "Done calibrating left camera." << endl;
    cout << "Calibrating right camera..." << endl;
    Values rightResult = calibrateMonoCamera(corners3D, rightCorners2D);
    cout << "Done calibrating right camera." << endl;
    cout << endl;

    /// set up stereo calibration
    cout << "Calibrating stereo cameras..." << endl;
    StereoCalibration stereoCalibration(nrImages);

    Cal3DS2 K0;
    Cal3DS2 K1;
    map<size_t, Pose3> leftCamPoses;
    Pose3 betweenPose;
    boost::tie(K0, K1, leftCamPoses, betweenPose) = stereoCalibration.init(leftResult, rightResult);

    /// stereo calibration
    Values result;
    NonlinearFactorGraph graph;
    vector<vector<Point2> > refinedLeftCorners;
    vector<vector<Point2> > refinedRightCorners;
    for (size_t i = 0; i < nrImages; i++) {
        vector<Point2> tempLeftCorners;
        vector<Point2> tempRightCorners;
        if (leftCamPoses.find(i) != leftCamPoses.end()) {
            assert(leftCorners2D[i].size() == rightCorners2D[i].size());
            for (size_t j = 0; j < leftCorners2D[i].size(); j++) {
               Point2 leftP = leftResult.at<Point2>(Z(i*40+j));
               tempLeftCorners.push_back(leftP);

               Point2 rightP = rightResult.at<Point2>(Z(i*40+j));
               tempRightCorners.push_back(rightP);
            }
        }
        refinedLeftCorners.push_back(tempLeftCorners);
        refinedRightCorners.push_back(tempRightCorners);
    }
    boost::tie(result, graph) = stereoCalibration.calibrateCameras(K0, K1, leftCamPoses, betweenPose,
                                                                   corners3D, refinedLeftCorners, refinedRightCorners);
    Marginals marginals(graph, result);
    cout << "Done calibrating stereo cameras." << endl;
    cout << endl;

    double error = graph.error(result) / graph.size();
    cout << "Average reprojection error is " << error << endl;
    cout << endl;

    Matrix KK0 = result.at<Cal3DS2>(K(0)).K();
    Vector kk0 = result.at<Cal3DS2>(K(0)).k();
    cout << "Left camera intrinsics: \n" << KK0 << endl;
    cout << "Left camera distortion: \n" << kk0 << endl;
    cout << "Covariance of left camera calibration is \n" << marginals.marginalCovariance(K(0)) << endl;
    cout << endl;

    Matrix KK1 = result.at<Cal3DS2>(K(1)).K();
    Vector kk1 = result.at<Cal3DS2>(K(1)).k();
    cout << "Right camera intrinsics: \n" << KK1 << endl;
    cout << "Right camera distortion: \n" << kk1 << endl;
    cout << "Covariance of right camera calibration is \n" << marginals.marginalCovariance(K(1)) << endl;
    cout << endl;

    Pose3 left_P_right = result.at<Pose3>(B(0));
    cout << "Between pose: " << endl;
    cout << "Rotation: \n" << left_P_right.rotation().matrix() << endl;
    cout << "Translation: \n" << left_P_right.translation().vector() << endl;
    cout << "Covariance of between pose is \n" << marginals.marginalCovariance(B(0)) << endl;

    writeSetereoCalibrationResults(resultFilename, result);

    return 0;
}



