/**
 * @file    Yao01_CornerDetection.cpp
 * @brief   Example of extracting corners
 * @author  Yao Chen
 * @date    July 18, 2016
 */

#include "../CornerDetector.h"
#include "../Utils.h"

using namespace std;
using namespace gtsam;

int main() {
    int rows = 6;
    int cols = 7;
    int nrSkippedImage = 0;
    int nrImages = 8;
//    const string imageDir = "/home/yao/data/auto-rally-car/platformA_2015-12-06-11-05-26/left_camera/image_color/";
//    const string imageDir = "/home/yao/data/auto-rally-car/platformA_2015-12-06-11-08-16/left_camera/image_color/";
    const string imageDir = "/Users/yaochen/data/calibration/";
    const string filenamePre = "Image";
    const string filenameExt = ".JPG";
    int filenameNumWidth = 2;


    CornerDetector cornerDetector(rows, cols, nrSkippedImage, nrImages, imageDir, filenamePre, filenameExt, filenameNumWidth);
    vector<vector<Point2> > corners = cornerDetector.detectCorners();
    cornerDetector.visualize(corners, 2);
//    writeCorners2D(imageDir + "corners2DOpenCV.txt", corners);

    return 0;
}
