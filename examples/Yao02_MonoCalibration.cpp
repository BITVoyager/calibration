/**
 * @file    Yao02_MonoCalibration.cpp
 * @brief   Example of monocular calibration
 * @author  Yao Chen
 * @date    July 18, 2016
 */

#include "../CornerDetector.h"
#include "../MonoCalibration.h"
#include "../Utils.h"

#include <gtsam/inference/Symbol.h>
#include <gtsam/linear/GaussianFactorGraph.h>
#include <gtsam/nonlinear/Marginals.h>

#include <iostream>

using namespace std;
using namespace gtsam;

using symbol_shorthand::K;
using symbol_shorthand::X;
using symbol_shorthand::Z;

int main() {
    int rows = 6;
    int cols = 7;
    size_t nrSkippedImage = 0;
    size_t nrImages = 8;
    //const string imageDir = "/home/yao/data/auto-rally-car/platformA_2015-12-06-11-05-26/left_camera/image_color/";
    //const string imageDir = "/home/yao/data/auto-rally-car/platformA_2015-12-06-11-08-16/left_camera/image_color/";
    const string imageDir = "/Users/yaochen/data/calibration/";
    const string filenamePre = "Image";
    const string filenameExt = ".JPG";
    int filenameNumWidth = 2;
    double spacing = 27.3;

    CornerDetector cornerDetector(rows, cols, nrSkippedImage, nrImages, imageDir, filenamePre, filenameExt, filenameNumWidth);
    vector<vector<Point2> > corners2D = cornerDetector.detectCorners();
    writeCorners2D(imageDir + "corners2DOpenCV.txt", corners2D);

    vector<Point2> corners3D;
    for (int y = cols - 1; y >= 0; y--) {
        for (int x = 0; x < rows; x++) {
            double px = spacing * static_cast<double>(x);
            double py = spacing * static_cast<double>(y);
            corners3D.push_back(Point2(px, py));
        }
    }
    writeCorners3D(imageDir + "corners3DOpenCV.txt", corners3D);

    MonoCalibration monoCalibration(nrImages);
    map<size_t, Matrix3> homographies = monoCalibration.estimateHomographies(corners3D, corners2D);
//    Matrix3 initK = monoCalibration.estimateIntrinsics2VanishingPoint(homographies);
    Matrix3 initK = monoCalibration.estimateIntrinsics(homographies);

    map<size_t, Pose3> initCamPoses = monoCalibration.estimateExtrinsics(initK, homographies);
    Values initial;
    Values result;
    NonlinearFactorGraph graph;
    boost::tie(initial, result, graph) = monoCalibration.calibrateCamera(initCamPoses, initK, corners3D, corners2D);
    cout << result.at<Cal3DS2>(K(0)).K() << endl;

    // Marginals marginals(graph, result);
    //
    // double error = graph.error(result) / graph.size();
    // graph.printErrors(result);
    // cout << "Average reprojection error is " << error << endl;
    // cout << endl;
    //
    // Matrix KK = result.at<Cal3DS2>(K(0)).K();
    // Vector kk = result.at<Cal3DS2>(K(0)).k();
    // cout << "Camera intrinsics: \n" << KK << endl;
    // cout << "Camera distortion: \n" << kk << endl;
    // cout << "Covariance of camera calibration is \n" << marginals.marginalCovariance(K(0)) << endl;
    // cout << endl;
    //
    // for (size_t i = 0; i < nrImages; i++) {
    //     if (result.find(X(i)) != result.end()) {
    //         Pose3 camPose = result.at<Pose3>(X(i));
    //         cout << "Camera pose " << i << ":" << endl;
    //         cout << "Rotation: \n" << camPose.rotation().matrix() << endl;
    //         cout << "Translation: \n" << camPose.translation().vector() << endl;
    //         cout << "Covariance of Pose " << i << " is \n" << marginals.marginalCovariance(X(i)) << endl;
    //     }
    // }

    // for (size_t i = 0; i < corners2D.size(); i++) {
    //     if (initCamPoses.find(i) != initCamPoses.end()) {
    //         for (size_t j = 0; j < corners2D[i].size(); j++) {
    //             cout << "Point " << j << " at frame " << i << result.at<Point2>(Z(i*40+j)) << endl;
    //             cout << "Covariance is " << marginals.marginalCovariance(Z(i*40+j)) << endl;
    //             cout << endl;
    //         }
    //     }
    // }



//    Ordering ordering;
//    ordering.push_back(K(0));
//    for (const auto& entry: initCamPoses) {
//        ordering.push_back(X(entry.first));
//    }

//    GaussianFactorGraph::shared_ptr gf = graph.linearize(result);

//    pair<Matrix, Vector> JacobianError = gf->jacobian(ordering);
//    Matrix J = JacobianError.first;
//    Matrix JJ = J.transpose() * J;
//    Matrix Sigma = JJ.inverse().block(9, 9, 6, 6);
//    cout << Sigma << endl;
//    cout << endl;

//    pair<Matrix, Vector> HessianError = gf->hessian(ordering);
//    Matrix JJ2 = HessianError.first;
//    Matrix Sigma2 = JJ2.block(0, 0, 9, 9).inverse();
//    cout << Sigma2 << endl;
//    cout << endl;

//    Matrix JJ3 = marginals.jointMarginalInformation((vector<Key>)ordering).fullMatrix();
//    cout << JJ3.block(0, 0, 9, 9).inverse() << endl;

    return 0;
}
