/**
 * @file    Yao03_StereoCalibration.cpp
 * @brief   Example of stereo calibration
 * @author  Yao Chen
 * @date    July 19, 2016
 */

#include "../CornerDetector.h"
#include "../MonoCalibration.h"
#include "../StereoCalibration.h"

#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/Marginals.h>

#include <chrono>
#include <ctime>
#include <iostream>

using namespace std;
using namespace gtsam;

using symbol_shorthand::B;
using symbol_shorthand::K;
using symbol_shorthand::X;

Values calibrateMonoCamera(const vector<Point2>& corners3D, const vector<vector<Point2> >& corners2D) {
    MonoCalibration monoCalibration(corners2D.size());
    map<size_t, Matrix3> homographies = monoCalibration.estimateHomographies(corners3D, corners2D);
    Matrix3 initK = monoCalibration.estimateIntrinsics2VanishingPoint(homographies);
    map<size_t, Pose3> initCamPoses = monoCalibration.estimateExtrinsics(initK, homographies);
    Values initial;
    Values result;
    NonlinearFactorGraph graph;
    boost::tie(initial, result, graph) = monoCalibration.calibrateCamera(initCamPoses, initK, corners3D, corners2D);
    return result;
}

int main() {
    /// set parameters
    int rows = 5;
    int cols = 8;
    int nrSkippedImage = 0;
    int nrImages = 80;
    const string leftImageDir = "/home/yao/data/auto-rally-car/platformA_2015-12-06-11-05-26/left_camera/image_color/";
    const string rightImageDir = "/home/yao/data/auto-rally-car/platformA_2015-12-06-11-05-26/right_camera/image_color/";
    const string filenamePre = "img";
    const string filenameExt = ".jpg";
    int filenameNumWidth = 6;
    double spacing = 83.2;

    /// detect corners
    CornerDetector leftCornerDetector(rows, cols, nrSkippedImage, nrImages, leftImageDir, filenamePre, filenameExt, filenameNumWidth);
    vector<vector<Point2> > leftCorners2D = leftCornerDetector.detectCorners();

    CornerDetector rightCornerDetector(rows, cols, nrSkippedImage, nrImages, rightImageDir, filenamePre, filenameExt, filenameNumWidth);
    vector<vector<Point2> > rightCorners2D = rightCornerDetector.detectCorners();

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    /// create corners on the calibration rig
    vector<Point2> corners3D;
    for (int y = cols - 1; y >= 0; y--) {
        for (int x = 0; x < rows; x++) {
            double px = spacing * static_cast<double>(x);
            double py = spacing * static_cast<double>(y);
            corners3D.push_back(Point2(px, py));
        }
    }

    /// monocular calibrations
    assert(nrImages == leftCorners2D.size());
    assert(nrImages == rightCorners2D.size());
    Values leftResult = calibrateMonoCamera(corners3D, leftCorners2D);
    Values rightResult = calibrateMonoCamera(corners3D, rightCorners2D);

    /// set up stereo calibration
    StereoCalibration stereoCalibration(nrImages);

    Cal3DS2 K0;
    Cal3DS2 K1;
    map<size_t, Pose3> leftCamPoses;
    Pose3 betweenPose;
    boost::tie(K0, K1, leftCamPoses, betweenPose) = stereoCalibration.init(leftResult, rightResult);

    /// stereo calibration
    Values result;
    NonlinearFactorGraph graph;
    boost::tie(result, graph) = stereoCalibration.calibrateCameras(K0, K1, leftCamPoses, betweenPose,
                                                                   corners3D, leftCorners2D, rightCorners2D);
    Marginals marginals(graph, result);
    end = std::chrono::system_clock::now();


    /// show results
    std::chrono::duration<double> elapsed_seconds = end-start;
    cout << "Calibration takes " << elapsed_seconds.count() << "s" << endl;

    double error = graph.error(result) / graph.size();
    cout << "Average reprojection error is " << error << endl;
    cout << endl;

    Matrix KK0 = result.at<Cal3DS2>(K(0)).K();
    Vector kk0 = result.at<Cal3DS2>(K(0)).k();
    cout << "Left camera intrinsics: \n" << KK0 << endl;
    cout << "Left camera distortion: \n" << kk0 << endl;
    cout << "Covariance of left camera calibration is \n" << marginals.marginalCovariance(K(0)) << endl;
    cout << endl;

    Matrix KK1 = result.at<Cal3DS2>(K(1)).K();
    Vector kk1 = result.at<Cal3DS2>(K(1)).k();
    cout << "Right camera intrinsics: \n" << KK1 << endl;
    cout << "Right camera distortion: \n" << kk1 << endl;
    cout << "Covariance of right camera calibration is \n" << marginals.marginalCovariance(K(1)) << endl;
    cout << endl;

    Pose3 bPose = result.at<Pose3>(B(0));
    cout << "Between pose: " << endl;
    cout << "Rotation: \n" << bPose.rotation().matrix() << endl;
    cout << "Translation: \n" << bPose.translation().vector() << endl;
    cout << "Covariance of between pose is \n" << marginals.marginalCovariance(B(0)) << endl;

    return 0;
}



