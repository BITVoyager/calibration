/**
 * @file    Yao05_CompareMonoCalibration.cpp
 * @brief   Example of comparing monocular calibrations
 * @author  Yao Chen
 * @date    August 18, 2016
 */

#include "../CornerDetector.h"
#include "../MonoCalibration.h"
#include "../Utils.h"

#include <gtsam/inference/Symbol.h>
#include <gtsam/linear/GaussianFactorGraph.h>
#include <gtsam/nonlinear/Marginals.h>

#include <iostream>

using namespace std;
using namespace gtsam;

using symbol_shorthand::K;
using symbol_shorthand::X;

int main() {
    int rows = 5;
    int cols = 8;
    size_t nrImages = 37;

    const string corner2DFilename ="/home/yao/data/auto-rally-car/calibration/test_corner/corners2DMATLAB.txt";
    vector<vector<Point2> > corners2D = readCorners2D(corner2DFilename, rows * cols);
    const string corner3DFilename = "/home/yao/data/auto-rally-car/calibration/test_corner/corners3DMATLAB.txt";
    vector<Point2> corners3D = readCorners3D(corner3DFilename);

    MonoCalibration monoCalibration(nrImages);
    map<size_t, Matrix3> homographies = monoCalibration.estimateHomographies(corners3D, corners2D);
    Matrix3 initK = monoCalibration.estimateIntrinsics2(homographies);

    map<size_t, Pose3> initCamPoses = monoCalibration.estimateExtrinsics(initK, homographies);
    Values initial;
    Values result;
    NonlinearFactorGraph graph;
    boost::tie(initial, result, graph) = monoCalibration.calibrateCamera(initCamPoses, initK, corners3D, corners2D);   

    double error = graph.error(result) / graph.size();
    cout << "Average reprojection error is " << error << endl;
    cout << endl;

    Matrix KK = result.at<Cal3DS2>(K(0)).K();
    Vector kk = result.at<Cal3DS2>(K(0)).k();
    Marginals marginals(graph, result);
    cout << "Camera intrinsics: \n" << KK << endl;
    cout << "Camera distortion: \n" << kk << endl;
    cout << "Covariance of camera calibration is \n" << marginals.marginalCovariance(K(0)) << endl;
    cout << endl;

    for (size_t i = 0; i < nrImages; i++) {
        if (result.find(X(i)) != result.end()) {
            Pose3 camPose = result.at<Pose3>(X(i));
            cout << "Camera pose " << i << ":" << endl;
            cout << "Rotation: \n" << camPose.rotation().matrix() << endl;
            cout << "Translation: \n" << camPose.translation().vector() << endl;
            cout << "Covariance of Pose " << i << " is \n" << marginals.marginalCovariance(X(i)) << endl;
        }
    }

    graph.printErrors(result);

    return 0;
}


