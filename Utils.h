/**
 * @file    Utils.h
 * @brief   Utility functions
 * @author  Yao Chen
 * @date    August 16, 2016
 */

#pragma once

#include <gtsam/geometry/Point2.h>
#include <gtsam/nonlinear/Values.h>

#include <string>

/**
 * @brief   Write stereo calibration results into a ROS launch file
 * @param   filename: the output filename
 * @param   result: calibration result
 */
void writeSetereoCalibrationResults(const std::string& filename, const gtsam::Values& result);

/**
 * @brief   Read corners in images from file
 * @param   filename: the name of the file to be read
 * @param   size: the number of corners on the calibration rig
 * @return  All corners in images
 */
std::vector<std::vector<gtsam::Point2> > readCorners2D(const std::string& filename, int size);

/**
 * @brief   Write corners in images to file
 * @param   filename: the name of the file to be read
 * @return  Success or not
 */
bool writeCorners2D(const std::string& filename, const std::vector<std::vector<gtsam::Point2> > &corners);

/**
 * @brief   Read corners on the calibration rig from file
 * @param   filename: the name of the file to be read
 * @return  All corners on calibration rig
 */
std::vector<gtsam::Point2> readCorners3D(const std::string& filename);

/**
 * @brief   Write corners on the calibration rig to file
 * @param   filename: the name of the file to be read
 * @return  Success or not
 */
bool writeCorners3D(const std::string& filename, const std::vector<gtsam::Point2>& corners);
